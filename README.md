# Kraken Monitor

Kraken Monitor is the tool developed under the AITC Summer Research scholarship for the control of 
processes spawned by Kraken. This is a GUI interface that allows the pausing, starting, stepping and 
stopping of processes. The code files are written in Python with PySide2 bindings to Qt 5.

Currently, the tabbed interface has two completed pages. The details view will allow advanced users 
to see all the properties of each process. The processes view is a more user friendly view that 
displays relevant properties for each process group.

The interface connects to a Kraken Server backend to retrieve data from a host machine running the 
processes. The communication between frontend and backend is through RPC with ``aiomas``. msgpack
encoding with blosc compression is used.

This tool was authored by Jerry Fan.

## Quickstart

Make sure you have Python 3.7+ installed. Kraken Server should be running on a machine that has 
OCEAN setup, and the Kraken Manager should have been used to start the processes.

### Qt Installation

The Kraken Monitor GUI is powered by Qt 5.12+. Installation of Qt is thus required. See 
[Qt Downloads](https://www.qt.io/download-open-source) for instructions on installing Qt.

### Python Packages

Install the required pip packages by running:

```
pip install -r requirements.txt
```

If developing this tool, install the dev dependencies by running:

```
pip install -r requirements-dev.txt
```

### Running the Application

To start the application, simply provide the ``run_app`` script with the hostname of a server 
running Kraken Server:

```
python run_app.py
```

Additional options can be accessed via ``--help``:

```
usage: run_app.py [-h] [-p REMOTE_PORT] -a REMOTE_HOST [-b]
                [-i POLLING_INTERVAL] [-v] [-vv] [-q] [--log-file LOG_FILE]

Kraken Monitor - GUI application for Kraken process monitoring and control.

optional arguments:
-h, --help            show this help message and exit
-p REMOTE_PORT, --remote-port REMOTE_PORT
                        Specify RPC server port.
-a REMOTE_HOST, --remote-host REMOTE_HOST
                        Specify RPC server bind hostname.
-i POLLING_INTERVAL, --polling-interval POLLING_INTERVAL
                        Frequency to poll for process data (e.g. 1 s).
                        Recommended > 0.05.
-v, --verbose         Enable logging (logging.INFO).
-vv, --extra-verbose  Enable debug logging (logging.DEBUG).
-q, --quiet           Disable logging (logging.CRITICAL).
--log-file LOG_FILE   Specify a log file to stream logging data.
```


Additional documentation is available through the ``Sphinx`` generated HTML pages.
