"""Provides the base class for list models used by Kraken Monitor.

The model provides data storage for a list view or table view. It is able to support remove and 
add row operations, as well as data change operations.

"""


import abc
from PySide2.QtCore import QAbstractListModel, QModelIndex, Qt

class KrakenMonitorListModel(QAbstractListModel):
    """Base class for list models used in Kraken Monitor.

    This base class will provide subclasses with an implemention of row removal and insertion, as 
    well as row update methods. Roles are used as columns, so these must be defined through the
    ``define_roles`` method. The constructor will call this method to setup the roles in the object 
    and also expose a ``role`` and ``role_names`` dictionary for subclasses to refer to if a 
    row lookup is ever needed.
    
    Args:
        data (list): List containing dictionary-like structures of data. Defaults to [].
        parent (QObject, optional): Parent of this QObject. Defaults to None.

    Attributes:
        qt_roles (dict): Dictionary with key-value pairs of role number and role name in binary 
            string.
        roles (dict): Dictionary translated from ``qt_roles`` containing key-value pairs of role 
            number and role name in normal string.
        role_names (dict): Dictionary translated from ``roles`` containing key-value pairs of 
            normal string and role number. This is a reverse lookup table for roles.
        model_data (list): List of dictionaries that function as the data storage for the model. 
            List dictionary items should contain the same keys as the model's ``role_names``.

    """
    ModelData = Qt.UserRole  # Set the first role int to UserRole

    def __init__(self, data=[], parent=None):
        QAbstractListModel.__init__(self, parent)

        self.qt_roles = self.define_roles()
        self.qt_roles[KrakenMonitorListModel.ModelData] = b"modelData"
        self.roles = dict([(key, value.decode("utf-8", "ignore")) for key, value in self.qt_roles.items()])
        self.role_names = ivd = {v: k for k, v in self.roles.items()}
        self.model_data = data

    @abc.abstractmethod
    def define_roles(self):
        """Returns the defined roles for this model.

        This method is used in initialisation to assign the dictionaries needed to define roles. 
        Subclasses must implement this function correctly by returning a dictionary with the 
        correct format.
        
        Returns:
            dict: The key-value pairs of role number and role name in binary string.
            
        """
        return {}

    def roleNames(self):
        """Overrides Qt ``roleNames``.
        
        Returns:
            dict: Key-value pairs of role number and role name in binary string.

        """
        roles = self.qt_roles
        return roles

    def rowCount(self, index):
        """Overrides Qt ``rowCount``.
        
        Args:
            index (QModelIndex): Index value representing the column that holds the rows. Ignored 
                for ``QAbstractListModel``.
        
        Returns:
            int: Number of items currently in ``model_data``.

        """
        return len(self.model_data)

    def data(self, index, role):
        """Overrides Qt ``data``.
        
        Args:
            index (QModelIndex): Index value representing the row to update. Column should be 0.
            role (int): The role number to get data of.
        
        Returns:
            Any: The data contained at the index with the specified role.

        """
        d = self.model_data[index.row()]

        if role == KrakenMonitorListModel.ModelData:
            return d
        elif role in self.qt_roles:
            return d[self.roles[role]]

        return None

    def set_data(self, index, role, data):
        """Sets the data at a specified index and role.
        
        Args:
            index (QModelIndex): Index value representing the row to update. Column should be 0.
            role (int): The role number to set data for.
            data (Any): The data to place.
        """
        d = self.model_data[index.row()]

        # Either grab the whole item or a specified role (dictionary key) in the item.
        if role == KrakenMonitorListModel.ModelData:
            self.model_data[index.row()] = data
        elif role in self.qt_roles:
            d[self.roles[role]] = data

        self.dataChanged.emit(index, index, [role, KrakenMonitorListModel.ModelData])

    def insert_rows(self, index, rows):
        """Inserts rows at a specified index.
        
        Args:
            index (int): The column to insert rows at. Rows will be inserted after the specified 
                index.
            rows (list): The rows to insert.

        """
        if len(rows) > 0:
            index_end = index + len(rows) - 1
            self.beginInsertRows(QModelIndex(), index, index_end)
            self.model_data[index:index] = rows
            self.endInsertRows()

    def remove_rows(self, index, count):
        """Removes rows at a specified index.
        
        Args:
            index (int): The column to start removing rows at. Row removal is inclusive.
            count (int): Number of rows after the index to remove (inclusive of the index)
    
        """
        if count > 0:
            index_end = index + count - 1
            self.beginRemoveRows(QModelIndex(), index, index_end)
            del self.model_data[index:index + count]
            self.endRemoveRows()

    def refresh(self):
        """Refreshes the attached view, forcing a redraw.

        This method is used when the view is suspected to be out of sync, or if a large data update 
        was performed.

        """
        self.beginResetModel()
        self.endResetModel()
