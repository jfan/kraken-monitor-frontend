"""Provides the base class for data connections.

Connections bring in data that the monitor can display. Usually these are from servers. Connection 
objects would connect to the server and grab the relevant data per data interval.

"""


import abc
from PySide2.QtCore import QObject

class KrakenMonitorConnection(QObject):
    """Base class for connections

    This class is intended to be used as a starting point for creating a connection subclass that
    would provide data from a server to a bridge object. The connection would emit signals that 
    are connected to the bridge, and drive data updates per data interval. Subclasses must 
    implement ``set_data_interval``, ``start`` and ``stop`` methods.

    Args:
        parent (QObject, optional): Parent of this QObject. Defaults to None.
    
    """
    def __init__(self, parent=None):
        super().__init__(parent)

    @abc.abstractmethod
    def set_data_interval(self, interval):
        """Sets the data retrieval rate.
        
        Args:
            interval (float): The time in seconds between data retrievals.
        
        Returns:
            bool: True if successful, False if not.
        
        """
        return False

    @abc.abstractmethod
    def start(self, *args, **kwargs):
        """Starts the connection.

        This method should start an async event loop that will retrieve data from the server at the 
        defined data interval rate.

        Args:
            *args: Additional position based connection arguments.
            **kwargs: Additional name based connection arguments.
        
        Returns:
            bool: True if successful, False if not.
        
        """
        return False

    @abc.abstractmethod
    def stop(self, blocking=True):
        """Stops the connection

        This method should stop the event loop that was started.
        
        Args:
            blocking (bool, optional): Indicates whether the method will block execution 
                until the connection stops or whether stopping is performed asynchronously. 
                Defaults to True.
        
        Returns:
            bool: True if successful, False if not.
        
        """
        return False
