""" Provides the base classes for bridges

Bridges are used as a backing for QML items. They facilitate communication from QML views into 
Python.

"""

import abc
from PySide2.QtCore import QObject

class KrakenMonitorBridge(QObject):
    """Base class for bridges.
    
    Provides backing for QML pages in QtQuick. Slots, signals and properties can be attached to
    bridges such that they are usable from QML. Bridges should be started when the relevant Qt item 
    is created, stopped when the Qt item is destroyed, paused when the item is invisible, 
    and resumed when the item is visible.

    Subclasses must implement ``start``, ``stop``, ``pause`` and ``resume`` methods.

    Args:
        parent (QObject, optional): Parent of this QObject. Defaults to None.

    """
    def __init__(self, parent=None):
        """Initialises the bridge."""
        super().__init__(parent)
        
    @abc.abstractmethod
    def start(self, paused=False):
        """Starts execution of the bridge. 
        
        This method may start an additional QThread to run its event loop, thereby keeping the GUI 
        responsive during processing.
        
        Args:
            paused (bool, optional): Start in paused mode. Defaults to False.
        
        Returns:
            bool: True if successful, False if not.
        
        """
        return False

    @abc.abstractmethod
    def stop(self, blocking=True):
        """Stops execution of the bridge.

        This method would close any QThreads started and dispose of any owned objects.

        Args:
            blocking (bool, optional): Indicates whether the method will block execution 
                until the bridge stops or whether stopping is performed asynchronously. Defaults to 
                True.

        Returns:
            bool: True if successful, False if not.
        
        """
        return False

    @abc.abstractmethod
    def pause(self):
        """Pauses execution of the bridge.

        This method should pause the event loop of the bridge if one exists.
        
        Returns:
            bool: True if successful, False if not.

        """
        return False

    @abc.abstractmethod
    def resume(self):
        """Resumes execution of the bridge.

        This method should pause the event loop of the bridge if one exists.
        
        Returns:
            bool: True if successful, False if not.
        
        """
        raise False
