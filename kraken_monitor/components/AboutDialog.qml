import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Dialog {
    modal: true
    standardButtons: Dialog.Ok

    background: Rectangle {
        color: Material.accent
    }
    Item {
        anchors.fill: parent
        Material.foreground: "white"

        RowLayout {
            spacing: 15
            anchors.fill: parent
            Item {
                Layout.fillHeight: true
                Layout.preferredWidth: 250
                Layout.margins: 15
                Image {
                    anchors.centerIn: parent
                    width: parent.width < parent.height ? parent.width : parent.height
                    height: width
                    source: "assets/octopus.svg"
                }
            }
            ColumnLayout {
                Layout.fillHeight: true
                Layout.margins: 15
                Label {
                    Layout.fillWidth: true
                    width: parent.width
                    wrapMode: TextArea.WordWrap
                    text: "<b>" + qsTr("Kraken") + "</b>" + qsTr(" Monitor")
                    font.pixelSize: 42
                }
                Label {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    width: parent.width
                    wrapMode: TextArea.WordWrap
                    text: "<b>Version 1.0.0</b><br/><br/>Created by Jerry Fan, University of Auckland.<br/>Email: j.fan@auckland.ac.nz<br/><br/>The Kraken Monitor application was created as part of a summer research scholarship at the Australian National University as an extension for Kraken."
                    font.pixelSize: 16
                }
                Label {
                    Layout.fillWidth: true
                    width: parent.width
                    wrapMode: TextArea.WordWrap
                    text: "This application uses various open source software libraries. For more information on licensing, see the included license file.\nKraken logo: Water vector created by freepik - www.freepik.com"
                    font.pixelSize: 12
                }
            }
        }
    }

    footer: DialogButtonBox {
        Material.foreground: "white"
        standardButtons: DialogButtonBox.AcceptRole
        background: Rectangle {
            color: Material.shade(Material.accent, Material.Shade700)
        }
    }
}
