/*** Provides a tabview for displaying dashboard pages.

***/


import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

ColumnLayout {
    default property list<Item> pages
    anchors.fill: parent
    spacing: 0

    TabBar {
        id: tabBar
        Layout.fillWidth: true
        focusPolicy: Qt.TabFocus

        Repeater {
            model: pages.length
            TabButton { text: pages[index].objectName }
        }
    }

    StackLayout {
        Layout.fillWidth: true
        currentIndex: tabBar.currentIndex
        data: pages
    }
}
