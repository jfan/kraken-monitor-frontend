"""Provides classes for the share component.

The classes in this module provide backing and control of the model for the Share.qml view.

"""


from PySide2.QtCore import Slot, Property, QObject, Signal, QThread
from PySide2.QtGui import QGuiApplication
import logging
from deepdiff import DeepDiff
from operator import itemgetter
import traceback
import time
from threading import Thread, Lock
from itertools import groupby
from kraken_monitor.base import KrakenMonitorBridge
from os import path
import os
import subprocess
from ansi2html import Ansi2HTMLConverter
from tempfile import NamedTemporaryFile
import json
import socket
from .share_file_manager import ShareFileManager


class ShareBridgeDataHandler(QObject):
    """Handles incoming data from a connection.

    Contains methods that can build and return function objects to update the view model. The
    data handler should be moved to a QThread when instantiated using its inherited
    ``moveToThread`` method.

    Args:
        shared_lock (Lock): Lock used for model update synchrononisation. Locked by the data
            handler when processing and unlocked by the bridge after update ops are performed.
        model (kraken_monitor.base.KrakenMonitorListModel): The model used in the ``ShareBridge``.
        parent (QObject, optional): Parent of this object. Defaults to None.

    Attributes:
        log (logging.Logger): The logger object.
        connection (kraken_monitor.base.KrakenMonitorConnection): The connection object.
        kernels_model (kraken_monitor.base.KrakenMonitorListModel): The model used in the
            ``ShareBridge`` for kernels.
        logs_model (kraken_monitor.base.KrakenMonitorListModel): The model used in the
            ``ShareBridge`` for logs.
        signals (ShareBridgeDataHandler.Signals): Signals used by the data handler.
        running (bool): Indicates whether the handler is running.
        paused (bool): Indicates whether the handler is paused.
        lock (Lock): Lock used for model update synchrononisation. Locked by the data handler when
            processing and unlocked by the bridge after update ops are performed.
        ansi2html (Ansi2HTMLConverter): Converter used on log file text.
        kernel_connect_requests (set): Connect requests are placed here and serviced the next time
            the kernel file is retrieved.

    """
    class Signals(QObject):
        """Signals used by ``ShareBridgeDataHandler``.

        Attributes:
            incoming_data (dict): Incoming digests passed through this signal.
            error (str): Handles error messages.
            model_updates (list): List of (func, [args]) update ops for use in the GUI thread.
            content_text_append (str, str): Signals available text to append to the text box.
            content_text_replace (str, str): Signals available text to replace for the text box.

        """
        incoming_data = Signal(dict)  # Received digest
        error = Signal(str)
        model_updates = Signal(list)  # Model update operations
        content_text_append = Signal(str, str)
        content_text_replace = Signal(str, str)

    def __init__(self, shared_lock, data_connection, logs_model, kernels_model, parent=None):
        super().__init__(parent)
        self.log = logging.getLogger(__file__)
        self.connection = data_connection
        self.kernels_model = kernels_model
        self.logs_model = logs_model
        self.signals = ShareBridgeDataHandler.Signals()
        self.running = False
        self.paused = False
        self.lock = Lock()  # Shared with main thread so that data update only goes through once
        self.ansi2html = Ansi2HTMLConverter()
        self.kernel_connect_requests = set()

    def get_index_ranges(self, data):
        """Helper method to group consecutive list elements.

        Args:
            data (list): List of integers to be grouped.

        Returns:
            list: List of tuples containing (start, end) of consecutive elements (inclusive).
        """
        ranges = []
        for k, g in groupby(enumerate(data), lambda x: x[0] - x[1]):
            group = (map(itemgetter(1), g))
            group = list(map(int, group))
            ranges.append((group[0], group[-1]))
        return ranges

    def update_model_ops(self, view_model, new_model):
        """Compares the current data in ``view_model`` with ``new_model`` and generates update ops.

        Used after the model generation step to compute the differences between existing and new
        models such that the view can be updated by the returned list of ops.

        Args:
            view_model (kraken_monitor.base.KrakenMonitorListModel): The ListModel to perform
                updates on.
            new_model (list): List of items that the view model will take as the new data.

        Returns:
            list: List of update operations as tuples of (func, [args]) that would transform the
                view model data into the new model.

        """

        ops = []
        old_model = view_model.model_data

        # Check item to see if anything has changed
        self.log.debug("Check for changes in model {}.".format(repr(view_model)))

        old_filenames = [item["filename"] for item in old_model]
        new_filenames = [item["filename"] for item in new_model]

        diff = DeepDiff(old_filenames, new_filenames, ignore_order=True)
        # # Deepdiff returns a dict item that is ordered by the values "root[0]", and we want the "0" only
        removals = [int(item[5:-1]) for item in diff["iterable_item_removed"].keys()] if "iterable_item_removed" in diff else []
        additions = [int(item[5:-1]) for item in diff["iterable_item_added"].keys()] if "iterable_item_added" in diff else []
        removals.sort()
        additions.sort()
        self.log.debug("Detected {} row removals ({}) and {} row additions ({}).".format(len(removals), removals, len(additions), additions))

        # Model updates for groups

        # Remove rows (in groups) from the right side
        removal_ranges = self.get_index_ranges(removals)
        for r in reversed(removal_ranges):
            index_start, index_end = r
            ops.append((view_model.remove_rows, (index_start, index_end - index_start + 1)))

        # Insert rows (in groups) from the left
        addition_ranges = self.get_index_ranges(additions)
        for r in addition_ranges:
            index_start, index_end = r
            items = new_model[index_start:index_end + 1]
            ops.append((view_model.insert_rows, (index_start, items)))

        self.log.debug("Checking property changes on untouched items.")

        # Check properties on untouched items to see if anything has changed
        additions_set = set(additions)
        removals_set = set(removals)
        new_items = [
            (i, item) for i, item in enumerate(new_model) if i not in additions_set
        ]
        old_items = [
            (i, item) for i, item in enumerate(old_model) if i not in removals_set
        ]

        for new, old in zip(new_items, old_items):
            new_index, new_item = new
            old_index, old_item = old

            self.log.debug("Checking item {}.".format(new_index))

            row_index = view_model.index(new_index)

            for key in view_model.role_names.keys():
                if key in new_item and not (new_item[key] == old_item[key]):
                    self.log.debug("{} property has changed, adding op.".format(key))
                    ops.append((view_model.set_data, (row_index, view_model.role_names[key], new_item[key])))

        return ops

    @Slot(dict)
    def run(self, digest):
        """Runs the processing pipeline to update the attached view.

        This method will reshape the digest data into a format that the model uses, then perform
        comparisons to generate the update operations needed to update the old model. A shared lock
        with the bridge object hosting the data handler is used to prevent multiple overlapping
        updates which may break the Qt view. This method should be connected to the incoming data
        signal. Will emit model update operations via signal.

        Args:
            digest (dict): The most recent digest.

        """
        if not self.paused:
            if self.lock.acquire(timeout=0):
                try:
                    self.log.debug("Running data handler method for change detection and update operations.")
                    t1 = time.perf_counter()
                    self.running = True
                    new_log_model = self.build_model(digest["logFiles"])
                    new_kernel_model = self.build_model(digest["kernelFiles"])
                    logs_update_ops = self.update_model_ops(self.logs_model, new_log_model)
                    kernels_update_ops = self.update_model_ops(self.kernels_model, new_kernel_model)
                    self.signals.model_updates.emit(logs_update_ops + kernels_update_ops)
                    t2 = time.perf_counter()
                    self.log.debug("Data handler completed in {} seconds.".format(t2 - t1))
                except Exception:
                    self.log.error("Exception occured in data handler method.")
                    self.signals.error.emit(traceback.format_exc())
                    self.lock.release()
                finally:
                    self.log.debug("Data handler finished.")
                    self.running = False
            else:
                self.log.info("Data handler overlapping, discarding.")

    def build_model(self, files):
        """Builds a data model that is used in the attached view.

        The data model used for the kernel and logs view is simply a list of dictionaries. This
        method sorts the digest items and gives a friendly name to the filenames.

        Args:
            files (list): List of files from the recent digest.

        Returns:
            list: The new data model.
        """

        self.log.debug("Building new model from digest data.")

        model_data = [
            {
                "filename": filename,
                "name": path.basename(filename)
            } for filename in files
        ]

        self.log.debug("New model built.")
        return model_data

    @Slot(bytes, str, str)
    def file_data_handler(self, data, filename, filetype):
        """Converts incoming file data into a readable format.

        Files will be parsed into utf-8 string format. Log files will have their color codes
        converted to html. Kernel files will be edited, saved, and displayed as plain text.

        Args:
            data (bytes): The chunk of data.
            filename (str): The full filename of the file.
            filetype (str): The filetype of the file.

        """
        try:
            if filetype == ShareFileManager.FileType.LOG:
                self.log.debug("Log file data received in handler, decoding.")
                text = self.ansi2html.convert(data.decode("utf-8", "ignore"))
                self.signals.content_text_append.emit(text, filename)
            elif filetype == ShareFileManager.FileType.KERNEL:
                self.log.debug("Kernel file data received in handler, decoding.")
                kernel_data = json.loads(data.decode("utf-8", "ignore"))
                if self.connection.worker is not None:
                    kernel_data["ip"] = socket.gethostbyname(self.connection.worker.remote_host)
                kernel_text = json.dumps(kernel_data, sort_keys=True, indent=4)
                text = self.ansi2html.convert(kernel_text)
                self.signals.content_text_replace.emit(text, filename)
                if filename in self.kernel_connect_requests:
                    self.kernel_connect(filename, kernel_text)
            self.log.debug("File handling complete.")
        except Exception:
            self.signals.error.emit(traceback.format_exc())

    def kernel_connect_request(self, kernel_filename):
        """Place a request in the requests set to be serviced.

        The connect request will be serviced when the kernel file is received by the handler.

        Args:
            kernel_filename (str): Full name of the kernel file.

        """
        if kernel_filename not in self.kernel_connect_requests:
            self.log.debug("New kernel connect request placed for {}.".format(kernel_filename))
            self.kernel_connect_requests.add(kernel_filename)

    def kernel_connect(self, kernel_filename, connection_text):
        """Launches an external terminal to connect to the kernel.

        This method is used internally to save the kernel data to disk so that jupyter can read
        the kernel connection file and connect.

        Args:
            kernel_filename (str): Full name of the kernel file.
            connection_text (str): The JSON connection info text.

        """
        if kernel_filename in self.kernel_connect_requests:
            self.log.debug("Servicing kernel connect request for {}. Connection info is:\n{}".format(kernel_filename, connection_text))
            self.kernel_connect_requests.remove(kernel_filename)

            # A new thread is needed to keep the temporary file alive
            def worker():
                with NamedTemporaryFile("r+", delete=False) as temp_file:
                    temp_file.write(connection_text)
                    temp_file.flush()
                    os.fsync(temp_file.fileno())

                    process = subprocess.Popen(["jupyter-qtconsole", "--existing", temp_file.name])
                    self.log.debug("New jupyter console launched with PID: {}".format(process.pid))
                    process.wait()

            thread = Thread(target=worker, daemon=True)
            thread.start()

    def pause(self):
        """Pause processing of data.

        Possibly used when the attached view is not visible to save CPU time.

        """
        self.paused = True

    def resume(self):
        """Resume processing of data.

        """
        self.paused = False


class ShareBridge(KrakenMonitorBridge):
    """Backs the ``Share.qml`` view.

    The bridge will provides methods that the view can run to process data, and provide event
    handlers to change the application behavior based on button clicks or other events in the view.
    The bridge also controls the lifecycle of its data handler.

    Args:
        data_connection (kraken_monitor.base.KrakenMonitorConnection): Data connection to use for
            retrieving digests and performing RPC calls.
        logs_model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by
            the bridge used for the logs list.
        kernels_model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by
            the bridge used for the kernel list.

    Attributes:
        column_resolvers (dict): Class attribute. Contains formatters for digest item properties so
            they can be pretty printed in the view.
        log (logging.Logger): The logger object.
        logs_model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by the bridge
            for the logs list.
        kernels_model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by the
            bridge for the kernels list.
        connection (kraken_monitor.base.KrakenMonitorConnection): The connection used by the bridge.
        thread (QThread): The thread used to run the data handler.
        file_manager_thread (QThread): The thread used to run the file manager.
        file_manager (ShareFileManager): The file manager object.
        data_handler (ShareBridgeDataHandler): The data handler object.
        paused (bool): Indicates whether the bridge is paused.
        lock (Lock): Lock for processing data and applying changes to the model.
        content_file (str): The file currently displayed in the content text.
        content_text (str): Text to be displayed in the content box.

    """
    def __init__(self, data_connection, logs_model, kernels_model):
        super().__init__()
        self.log = logging.getLogger(__file__)
        self.logs_model = logs_model
        self.kernels_model = kernels_model
        self.connection = data_connection
        self.component = None
        self.thread = None
        self.file_manager_thread = None
        self.file_manager = None
        self.data_handler = None
        self.paused = False
        self.lock = Lock()
        self.content_file = None
        self.__content_text = ""

    @Signal
    def content_text_changed(self):
        """Signal used when the content text changes.

        """
        pass

    def _content_text(self):
        """Returns the content text

        Returns:
            str: The text.

        """
        if self.content_file is not None:
            return self.__content_text
        return ""

    def set_content_text(self, text):
        """Set the content text

        Args:
            text (str): The text.
        """
        self.__content_text = text
        self.content_text_changed.emit()

    content_text = Property(str, _content_text, set_content_text, notify=content_text_changed)

    @Slot(str, str)
    def content_text_append(self, text, file):
        """Add to the content text.

        Callers must verify the current content file with the file argument.

        Args:
            text (str): The text to append
            file (str): The file to append text to.
        """
        if file == self.content_file:
            # Update text in chunks to ensure responsiveness
            step = 10000
            for i in range(0, len(text), step):
                self.content_text += text[i:i + step]
                QGuiApplication.processEvents()
            self.log.debug("Content text updated.")

    @Slot(str, str)
    def content_text_replace(self, text, file):
        """Replace the content text.

        Callers must verify the current content file with the file argument.

        Args:
            text (str): The text to replace with
            file (str): The file to replace text to.
        """
        if file == self.content_file:
            self.content_text = ""
            # Update text in chunks to ensure responsiveness
            step = 10000
            for i in range(0, len(text), step):
                self.content_text += text[i:i + step]
                QGuiApplication.processEvents()
            self.log.debug("Content text updated.")

    @Slot(list)
    def perform_model_update(self, ops):
        """Accepts a list of model update operations to perform.

        The model updates should have been deferred from the data handler.

        Args:
            ops (list): List of model update operations.

        """
        self.log.debug("Performing {} operations for model update.".format(len(ops)))
        if self.data_handler is not None and self.data_handler.lock.locked():
            try:
                t1 = time.perf_counter()
                for op in ops:
                    func, args = op
                    self.log.debug("Performing op {} with args {}.".format(func, args))
                    func(*args)
                t2 = time.perf_counter()
                self.log.debug("Model update performed in {} seconds.".format(t2 - t1))
            finally:
                # Ensure lock is released in any situation
                self.data_handler.lock.release()
        else:
            self.log.debug("Lock is not set, update not synchronised, discarding update.")

    @Slot(result=bool)
    def start(self, paused=False):
        """Starts the bridge and data handler.

        A ``QThread`` will be created to host the data handler operations.

        Args:
            paused (bool, optional): Start in paused mode. Defaults to False.

        Returns:
            bool: True if successful, False if not.

        """
        if self.data_handler is None:
            self.log.info("Starting Share Bridge data handler.")
            self.thread = QThread()
            self.file_manager_thread = QThread()
            self.data_handler = ShareBridgeDataHandler(self.lock, self.connection, self.logs_model, self.kernels_model)
            self.file_manager = ShareFileManager(self.connection)
            if paused:
                self.pause()
            self.data_handler.moveToThread(self.thread)
            self.data_handler.signals.error.connect(self.error_handler)
            self.data_handler.signals.model_updates.connect(self.perform_model_update)
            self.connection.signals.share_manager_digest_received.connect(self.data_handler.signals.incoming_data)
            self.data_handler.signals.incoming_data.connect(self.data_handler.run)
            self.data_handler.signals.content_text_append.connect(self.content_text_append)
            self.data_handler.signals.content_text_replace.connect(self.content_text_replace)
            self.thread.start()
            self.log.info("Data handling thread created and awaiting data.")
            self.file_manager.moveToThread(self.file_manager_thread)
            self.file_manager.signals.error.connect(self.error_handler)
            self.file_manager.signals.data_retrieved.connect(self.data_handler.file_data_handler)
            self.file_manager_thread.started.connect(self.file_manager.start)
            self.file_manager_thread.start()
            self.log.info("File manager thread created.")
            return True
        return False

    @Slot(result=bool)
    def stop(self, blocking=True):
        """Stops the bridge, data handler and file_manager.

        Args:
            blocking (bool, optional): If True, will block the calling thread until everything
            stops. Otherwise runs asynchronously. Defaults to True.

        Returns:
            Any: True if successful, False if not. The stopping thread if not blocking.

        """
        if self.data_handler is not None:
            def sequence():
                self.log.info("Shutting down file manager.")
                self.file_manager.signals.data_retrieved.disconnect()
                self.file_manager.signals.error.disconnect()
                self.log.info("Waiting for file manager to stop.")
                self.file_manager.stop()
                self.file_manager_thread.exit()
                self.file_manager_thread.wait(5000)
                self.file_manager = None
                self.file_manager_thread = None
                self.log.info("Data handler shutdown complete.")
                self.log.info("Shutting down data handler.")
                self.data_handler.signals.model_updates.disconnect()
                self.data_handler.signals.incoming_data.disconnect()
                self.data_handler.signals.content_text_append.disconnect()
                self.data_handler.signals.content_text_replace.disconnect()
                self.data_handler.signals.error.disconnect()
                self.log.info("Waiting for data handler thread to stop.")
                while self.data_handler.running:
                    time.sleep(0.5)
                    self.log.debug("Data handler status: {}.".format(self.data_handler.running))
                self.thread.exit()
                self.thread.wait(5000)
                self.data_handler = None
                self.thread = None
                self.log.info("Data handler shutdown complete.")
                return True

            if not blocking:
                stop_thread = Thread(target=sequence)
                stop_thread.start()
                return stop_thread
            else:
                return sequence()
        return False

    @Slot(str)
    def error_handler(self, message):
        """Handles error messages from the data handler.

        Args:
            message (str): The error message
        """
        self.log.error("Error from child thread: {}.".format(message))
        self.stop(blocking=False)

    @Slot(result=bool)
    def pause(self):
        """Pauses the bridge.

        Will pause the data handler.

        Returns:
            bool: True if successful, False if not.
        """
        if self.data_handler is not None and not self.paused:
            self.log.debug("Pausing data handler.")
            self.data_handler.pause()
            self.file_manager.clear_active_file()
            self.content_file = None
            self.content_text = ""
            self.paused = True
            return True
        return False

    @Slot(result=bool)
    def resume(self):
        """Resumes the bridge.

        Will resume the data handler.

        Returns:
            bool: True if successful, False if not.
        """
        if self.data_handler is not None and self.paused:
            self.log.debug("Resuming data handler.")
            self.data_handler.resume()
            self.paused = False
            return True
        return False

    @Slot(bool)
    def visible_changed_handler(self, visible):
        """Handler for QML visiblity change.

        Args:
            visible (bool): Current visiblity.
        """
        if visible:
            self.resume()
            try:
                self.connection.refresh_digest()
            except AssertionError:
                # Connection might not be ready
                pass
            self.logs_model.refresh()
            self.kernels_model.refresh()
        else:
            self.pause()

    @Slot(int)
    def log_activate_handler(self, index):
        """Event handler for double clicks on the log file rows.

        Will activate the file such that text is displayed in the content box.

        Args:
            index (int): The row index.

        """
        if index >= 0 and index < len(self.logs_model.model_data):
            filename = self.logs_model.model_data[index]["filename"]
            self.content_file = filename
            self.content_text = ""
            self.file_manager.set_active_file_async(filename, ShareFileManager.FileType.LOG)
            self.log.debug("Log file activated: {}".format(filename))

    @Slot(int)
    def kernel_connect_handler(self, index):
        """Places a connect request in the data handler.

        Args:
            index (int): The row index.

        """
        if index >= 0 and index < len(self.kernels_model.model_data):
            filename = self.kernels_model.model_data[index]["filename"]
            self.data_handler.kernel_connect_request(filename)
            self.log.debug("Kernel connect handler: {}".format(filename))

    @Slot(int)
    def kernel_activate_handler(self, index):
        """Event handler for double clicks on the kernel file rows.

        Will activate the file such that text is displayed in the content box.

        Args:
            index (int): The row index.

        """
        if index >= 0 and index < len(self.kernels_model.model_data):
            filename = self.kernels_model.model_data[index]["filename"]
            self.content_file = filename
            self.content_text = ""
            self.file_manager.set_active_file_async(filename, ShareFileManager.FileType.KERNEL)
            self.log.debug("Kernel file activated: {}".format(filename))
