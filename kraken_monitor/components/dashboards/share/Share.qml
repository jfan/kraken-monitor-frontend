import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4 as Legacy
import QtQuick.Controls.Material 2.12
import "../../../components"

Page {
    objectName: "Share"
    id: pageShare
    height: parent.height
    width: parent.width

    function getAllSelections(table) {
        var selections = []
        table.selection.forEach(function (rowIndex) {
            selections.push(rowIndex);
        })
        return selections
    }

    // Run on visiblity change
    function visibleChangedHandler() {
        shareBridge.visible_changed_handler(visible);
        if (!visible) {
            logFileView.selection.clear();
            kernelFileView.selection.clear();
        }
    }

    Component.onCompleted: {
        visibleChangedHandler();
    }

    onVisibleChanged: {
        visibleChangedHandler();
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        Rectangle {
            Layout.fillWidth: true
            height: 150
            Item {
                anchors.fill: parent
                anchors.leftMargin: 15
                anchors.rightMargin: 15
                anchors.topMargin: 10
                anchors.bottomMargin: 10
                // TODO: Make this a list view
                Legacy.TableView {
                    id: kernelFileView

                    function activateKernel() {
                        var selections = getAllSelections(kernelFileView);
                        if (selections.length === 1) {
                            shareBridge.kernel_connect_handler(selections[0]);
                            shareBridge.kernel_activate_handler(selections[0]);
                        }
                    }

                    anchors.fill: parent
                    enabled: dataConnection.connection_state === 0
                    selectionMode: Legacy.SelectionMode.SingleSelection
                    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                    model: shareKernelsModel
                    onClicked: {
                        logFileView.selection.clear();
                    }
                    onDoubleClicked: {
                        activateKernel();
                    }
                    Keys.onPressed: {
                        if (event.key === 16777220) {
                            activateKernel();
                        }
                    }

                    Legacy.TableViewColumn {
                        role: "name"
                        title: "Kernel files"
                        width: kernelFileView.width
                        resizable: false
                    }
                }
            }
        }
        Rectangle {
            Layout.fillWidth: true
            height: 150
            Item {
                anchors.fill: parent
                anchors.leftMargin: 15
                anchors.rightMargin: 15
                anchors.topMargin: 10
                anchors.bottomMargin: 10
                // TODO: Make this a list view
                Legacy.TableView {
                    id: logFileView

                    function activateLog() {
                        var selections = getAllSelections(logFileView);
                        if (selections.length === 1) {
                            shareBridge.log_activate_handler(selections[0]);
                        }
                    }

                    anchors.fill: parent
                    enabled: dataConnection.connection_state === 0
                    selectionMode: Legacy.SelectionMode.SingleSelection
                    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
                    model: shareLogsModel
                    onClicked: {
                        kernelFileView.selection.clear()
                    }
                    onDoubleClicked: {
                        activateLog();
                    }
                    Keys.onPressed: {
                        if (event.key === 16777220) {
                            activateLog();
                        }
                    }

                    Legacy.TableViewColumn {
                        role: "name"
                        title: "Log files"
                        width: logFileView.width
                        resizable: false
                    }

                }
            }
        }
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Item {
                anchors.fill: parent
                anchors.leftMargin: 15
                anchors.rightMargin: 15
                anchors.topMargin: 10
                anchors.bottomMargin: 10

                ColumnLayout {
                    anchors.fill: parent
                    spacing: 0

//                    Label {
//                        Layout.fillWidth: true
//                        id: contentsLabel
//                        text: qsTr("Contents")
//                        font.pixelSize: 18
//                        padding: 10
//                    }

                    Rectangle {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        color: "#000000"

                        Flickable {
                            id: contentTextFlickable
                            anchors.left: parent.left
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            anchors.right: contentTextScrollBar.left
                            clip: true
                            contentHeight: contentText.height
                            flickableDirection: Flickable.VerticalFlick
                            boundsBehavior: Flickable.StopAtBounds
                            ScrollBar.vertical: contentTextScrollBar
                            Item {
                                width: parent.width
                                TextEdit {
                                    property int prevStickContentY: 0;
                                    id: contentText
                                    enabled: dataConnection.connection_state === 0
                                    width: parent.width
                                    color: "#BBBBBB"
                                    textMargin: 5
                                    selectedTextColor: "#000000"
                                    selectionColor: "#BBBBBB"
                                    font.family: "Consolas, Droid Sans Mono, Inconsolata, Courier New, monospace"
                                    font.pixelSize: 16
                                    text: shareBridge.content_text
                                    textFormat: TextEdit.RichText
                                    wrapMode: TextEdit.WrapAnywhere
                                    selectByMouse: true
                                    readOnly: true
                                    onTextChanged: {
                                        if (contentTextFlickable.contentHeight > contentTextFlickable.height) {
                                            contentTextFlickable.contentY = contentTextFlickable.contentHeight - contentTextFlickable.height;
                                        }
                                    }
                                }
                            }
                        }
                        ScrollBar {
                            id: contentTextScrollBar
                            width: 20
                            background: Rectangle {
                                color: Material.color(Material.Grey, Material.Shade300)
                            }
                            anchors.top: parent.top
                            anchors.right: parent.right
                            anchors.bottom: parent.bottom
                            policy: ScrollBar.AlwaysOn
                            focusPolicy: Qt.WheelFocus
                        }
                    }
                }
            }
        }
    }
}
