"""Kraken kernel and log share

The share module contains the bridge, models and QML view for displaying all the process logs
and kernel connection files received by the manager. This data is displayed in two lists. Logs can
be viewed in the application whilst external consoles can be launched to connect to IPython
kernels.

Todo:
 * Switch the TableViews into ListViews
 * Find a way to clean up the cache when files are removed, currently they have a minor memory
   leak as old files are not removed (perhaps link to the digests).
 * Find a way to clean up the kernel connect requests if the kernel files have been removed.

"""


from .share_kernels_list_model import ShareKernelsListModel
from .share_logs_list_model import ShareLogsListModel
from .share_file_manager import ShareFileManager
from .share_bridge import ShareBridge
