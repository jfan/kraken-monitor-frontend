"""Downloads and manages files from the share agent.

"""


from PySide2.QtCore import QObject, Slot, Signal
from tempfile import TemporaryFile
from threading import Lock
import traceback
import logging
import asyncio
import time


class ShareFileManager(QObject):
    """The share file manager allows downloading a remote log or kernel file to be viewed locally.

    The manager will cache the current active file. Uses asyncio internally to manage tasks.

    Todo:
     * The lock is probably not implemented well, need better collision detection for file
        operations.

    Args:
        data_connection ([type]): [description]
        chunk_size (int, optional): [description]. Defaults to 1048576 (1 MiB).
        parent ([type], optional): [description]. Defaults to None.

    Attributes:
        log (logging.Logger): The logging object.
        signals (ShareFileManager.Signals): The signals used by the manager.
        connection (kraken_monitor.base.KrakenMonitorConnection): The connection object.
        files_cache (dict): The cache where file dictionary objects are stored. The dictionaries
            describe the file's filename, filetype and provides a temporary file to store its
            contents.
        lock (threading.Lock): Lock to prevent overlapping file syncs.
        active_file (str): The filename of the active file.
        chunk_size (int): The number of bytes to download in one request.
        running (bool): State of the file manager.
        loop (asyncio.loop): Contains the asyncio loop object.
        file_sync_loop_task (asyncio.Task): The file sync loop task started by this object.
        file_sync_task (asyncio.Task): The file sync task that might be running.
        data_handler_task (asyncio.Task): The data handler task that might be running.

    """
    class FileType(object):
        """Filetypes managed by the file manager.

        """
        KERNEL = "kernel"
        LOG = "log"

    class Signals(QObject):
        """Signals used in the file manager.

        Attributes:
            error (str): Error signal.
            data_retrieved (bytes, str, str): Data is ready for use.

        """
        error = Signal(str)
        data_retrieved = Signal(bytes, str, str)  # Data, filename, filetype

    def __init__(self, data_connection, chunk_size=1048576, parent=None):
        super().__init__(parent=None)
        self.log = logging.getLogger(__file__)
        self.signals = ShareFileManager.Signals()
        self.connection = data_connection
        self.files_cache = {}
        self.lock = Lock()
        self.active_file = None
        self.chunk_size = chunk_size
        self.running = False
        self.loop = None
        self.file_sync_loop_task = None
        self.syncing = False
        self.sync_task = None
        self.data_handler_task = None

    @Slot()
    def start(self):
        """Start the ShareFileManager and the file sync loop.

        The file sync loop keeps one active file up to date by storing new updates in a local cache.

        """
        try:
            if not self.running:
                try:
                    self.loop = asyncio.get_event_loop()
                except RuntimeError:
                    self.loop = asyncio.new_event_loop()

                asyncio.set_event_loop(self.loop)
                self.log.debug("Starting ShareFileManager.")
                self.files_cache = {}
                self.running = True

                self.file_sync_loop_task = asyncio.ensure_future(self.active_file_sync_loop())
                self.loop.run_until_complete(self.file_sync_loop_task)
        except Exception:
            self.signals.error.emit(traceback.format_exc())

    def stop(self):
        """Stops the file manager sync capablities.

        This method will cancel the request loop task and attempt a shutdown of the event loop.
        The method will block until the event loop stops.

        Returns:
            bool: True if successful, False if not.

        """
        if self.running:
            if self.file_sync_loop_task is not None:
                self.file_sync_loop_task.cancel()
            if self.sync_task is not None:
                self.sync_task.cancel()
            if self.data_handler_task is not None:
                self.data_handler_task.cancel()
            self.log.debug("Waiting for event loop shutdown.")
            while self.loop.is_running():
                time.sleep(0.5)
                self.log.debug("Event loop status: {}.".format(self.loop.is_running()))
            self.log.info("Cleaning up remaining tasks.")
            asyncio.set_event_loop(self.loop)
            remaining = asyncio.all_tasks(self.loop)
            self.loop.run_until_complete(asyncio.gather(*remaining))
            self.loop.close()
            self.log.debug("Event loop closed.")
            self.loop = None
            self.running = False
            for filename, entry in self.files_cache.items():
                entry["cache_file"].close()
            return True
        return False

    def set_active_file_async(self, filename, filetype):
        """Sets the active file for retrieval asynchronously.

        Will activate file syncing, pulling remote file contents into a local cache.

        Args:
            filename (str): The name of the file.
            filetype (str): Will be one of the filetypes from ```ShareFileManager.FileType```.

        Returns:
            bool: True if running. False if not.
        """
        if self.running:
            async def task():
                try:
                    if filename not in self.files_cache:
                        self.files_cache[filename] = {
                            "filetype": filetype,
                            "filesize": 0,
                            "cache_file": TemporaryFile(mode="wb+")
                        }
                    else:
                        if self.files_cache[filename]["filesize"] > 0:
                            self.files_cache[filename]["cache_file"].seek(0)
                            data = self.files_cache[filename]["cache_file"].read()
                            self.signals.data_retrieved.emit(data, filename, filetype)
                    self.active_file = filename
                    self.log.debug("Active file set as {}.".format(filename))
                    if self.sync_task is not None:
                        self.sync_task.cancel()
                        self.syncing = False
                    self.sync_task = asyncio.ensure_future(self.file_sync(filename), loop=self.loop)
                except asyncio.CancelledError:
                    self.log.debug("Set active file task cancelled.")
                except Exception:
                    self.signals.error.emit(traceback.format_exc())
            asyncio.ensure_future(task(), loop=self.loop)
            return True
        return False

    def clear_active_file(self):
        """Clears the currently active file.

        Will disable sync.

        """
        self.active_file = None
        self.log.debug("Cleared active file.")

    def data_chunk_handler_async(self, data, filename, from_byte):
        """Handles incoming data chunks asynchronously.

        This method will write incoming data into the cache at the appropriate byte location.

        Args:
            data ([type]): The data chunk.
            filename ([type]): The filename of the file the chunk is from.
            from_byte (int): The byte location from which the data chunk was requested.

        Returns:
            bool: True if running. False if not.

        """
        if self.running:
            async def task():
                if self.lock.acquire(blocking=False):
                    try:
                        entry = self.files_cache[filename]
                        if len(data) > 0:
                            if entry["filetype"] == ShareFileManager.FileType.KERNEL:
                                entry["cache_file"].truncate(0)
                            entry["cache_file"].seek(from_byte)
                            entry["cache_file"].write(data)
                            entry["filesize"] += len(data)
                            if filename == self.active_file:
                                self.signals.data_retrieved.emit(data, filename, entry["filetype"])
                                if entry["filetype"] == ShareFileManager.FileType.LOG:
                                    self.connection.read_log(filename, entry["filesize"], entry["filesize"] + self.chunk_size, lambda data: self.data_chunk_handler_async(data, filename, entry["filesize"]))
                        else:
                            self.syncing = False
                    except asyncio.CancelledError:
                        self.log.debug("Data chunk handler cancelled.")
                    except Exception:
                        self.signals.error.emit(traceback.format_exc())
                    finally:
                        self.lock.release()
            self.data_handler_task = asyncio.ensure_future(task(), loop=self.loop)
            return True
        return False

    async def active_file_sync_loop(self):
        """This method starts the file sync loop.

        """
        sync_task = None
        try:
            while True:
                if self.connection.worker is not None:
                    data_interval = self.connection.worker.data_interval

                    active_file = None
                    if self.active_file != active_file:
                        active_file = self.active_file
                        if sync_task is not None:
                            sync_task.cancel()
                    if active_file is not None and not self.syncing:
                        self.sync_task = asyncio.ensure_future(self.file_sync(active_file))

                    # Staggered sleep for responsiveness
                    if data_interval > 0.2:
                        i = 0
                        # We need the live value in case it changes TODO: Make this better and less reliant on the worker object
                        while self.connection.worker is not None and i < int(self.connection.worker.data_interval / 0.1):
                            await asyncio.sleep(0.1)
                            i += 1
                    else:
                        await asyncio.sleep(data_interval)
                else:
                    await asyncio.sleep(1)
        except asyncio.CancelledError:
            if sync_task is not None:
                sync_task.cancel()
        except Exception:
            self.signals.error.emit(traceback.format_exc())

    async def file_sync(self, filename):
        """This method attempts to download the specified file.

        Args:
            filename (str): The filename of the file.
        """
        try: 
            if not self.syncing:
                self.syncing = True
                if filename in self.files_cache:
                    self.log.debug("Syncing file {}.".format(filename))
                    entry = self.files_cache[filename]
                    if entry["filetype"] == ShareFileManager.FileType.LOG:
                        self.connection.read_log(
                            filename,
                            entry["filesize"],
                            entry["filesize"] + self.chunk_size,
                            lambda data: self.data_chunk_handler_async(data, filename, entry["filesize"])
                        )
                    elif entry["filetype"] == ShareFileManager.FileType.KERNEL:
                        self.connection.read_kernel_info(
                            filename,
                            lambda data: self.data_chunk_handler_async(data, filename, 0)
                        )
            else:
                self.log.debug("Already syncing.")
        except asyncio.CancelledError:
            self.syncing = False
            self.log.info("File sync canceled.")
        except Exception:
            self.syncing = False
            self.signals.error.emit(traceback.format_exc())
