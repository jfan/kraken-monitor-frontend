"""Provides the model used in the QML Share view for the logs list.

"""


from kraken_monitor.base import KrakenMonitorListModel


class ShareLogsListModel(KrakenMonitorListModel):
    """List of items representing columns as roles.

    The main data structure is a list of dictionaries. Each dictionary contains the roles as keys.
    Each dictionary represents a process log on the server.

    Args:
        data (list, optional): The list of dictionaries used as data. Defaults to [].
        parent (PySide2.QtCore.QObject, optional) The parent of this object. Defaults to None.

    """
    def __init__(self, data=[], parent=None):
        super().__init__(data, parent)

    def define_roles(self):
        """See ``kraken_monitor.base.KrakenMonitorListModel.define_roles``.

        """
        return dict((KrakenMonitorListModel.ModelData + 1 + i, role) for i, role in enumerate([
            b"filepath",
            b"name"
        ]))
