/*** Details View

Intended to be the advanced user view. This view displays processes with all available data in a 
table.

***/


import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls 1.4 as Legacy
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import "../../../components"

Page {
    objectName: "Details"
    id: pageDetails
    height: parent.height
    width: parent.width

    // Run on visiblity change
    function visibleChangedHandler() {
        detailsBridge.visible_changed_handler(visible);
        if (visible) {
            // Force focus on the table view first
            tableView.forceActiveFocus();
        }
        else {
            tableView.selection.clear();
        }
    }

    Component.onCompleted: {
        visibleChangedHandler();
    }

    onVisibleChanged: {
        visibleChangedHandler();
    }

    Legacy.TableView {
        id: tableView
        anchors.topMargin: 15
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: bottomBar.top
        selectionMode: Legacy.SelectionMode.ExtendedSelection
        enabled: dataConnection.connection_state === 0

        function getAllSelections() {
            var selections = []
            tableView.selection.forEach(function (rowIndex) {
                selections.push(rowIndex);
            })
            return selections
        }

        Legacy.TableViewColumn {
            role: "name"
            title: "Name"
            width: 230
        }
        Legacy.TableViewColumn {
            role: "pid"
            title: "PID"
            width: 80
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "status"
            title: "Status"
            width: 60
        }
        Legacy.TableViewColumn {
            role: "cpu"
            title: "CPU"
            width: 60
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "memory"
            title: "Memory"
            width: 100
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "loopCount"
            title: "Loop count"
            width: 60
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "control"
            title: "Control"
            width: 60
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "tmuxSession"
            title: "Tmux session"
            width: 230
        }
        Legacy.TableViewColumn {
            role: "loopStat"
            title: "Loop status"
            width: 60
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "statusCode"
            title: "Status code"
            width: 60
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "creationTime"
            title: "Creation time (UTC)"
            width: 150
        }
        Legacy.TableViewColumn {
            role: "cpuAffinity"
            title: "CPU affinity"
            width: 200
        }
        Legacy.TableViewColumn {
            role: "cpuContextSwitches"
            title: "Context switches"
            width: 100
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "threadCount"
            title: "Threads"
            width: 60
            horizontalAlignment: Text.AlignRight
        }
        Legacy.TableViewColumn {
            role: "message"
            title: "Message"
            width: 200
        }
        Legacy.TableViewColumn {
            role: "description"
            title: "Description"
            width: 200
        }
        model: detailsListModel
    }

    Item {
        id: bottomBar
        height: 60
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        RowLayout {
            anchors.fill: parent
            anchors.margins: 10
            spacing: 10

            Item {
                Layout.fillWidth: true
            }

            Button {
                id: runButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: 40
                enabled: dataConnection.connection_state === 0
                text: qsTr("Run")
                onClicked: {
                    detailsBridge.run_clicked_handler(tableView.getAllSelections());
                }
            }

            Button {
                id: stepButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: 40
                enabled: dataConnection.connection_state === 0
                text: qsTr("Step")
                onClicked: {
                    detailsBridge.step_clicked_handler(tableView.getAllSelections());
                }
            }

            Button {
                id: pauseButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: 40
                enabled: dataConnection.connection_state === 0
                text: qsTr("Pause")
                onClicked: {
                    detailsBridge.pause_clicked_handler(tableView.getAllSelections());
                }
            }

            Button {
                id: stopButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: 40
                enabled: dataConnection.connection_state === 0
                text: qsTr("Stop")
                onClicked: {
                    detailsBridge.stop_clicked_handler(tableView.getAllSelections());
                }
            }

            Button {
                id: killButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.preferredWidth: 100
                Layout.preferredHeight: 40
                enabled: dataConnection.connection_state === 0
                text: qsTr("Kill")
                onClicked: {
                    detailsBridge.kill_clicked_handler(tableView.getAllSelections());
                }
            }
        }

        CustomBorder {
            topBorderSize: 1
            borderColor: Material.color(Material.Grey)
        }
    }
}
