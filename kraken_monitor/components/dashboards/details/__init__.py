"""Advanced Kraken process properties.

The details module contains the bridge, model and QML view for displaying all the process 
information received by the manager. This data is displayed in a table and would allow advanced 
users to quickly glance at process statuses.

Todo:
 * Implement sorting on the columns.

"""

from .details_bridge import DetailsBridge
from .details_list_model import DetailsListModel
