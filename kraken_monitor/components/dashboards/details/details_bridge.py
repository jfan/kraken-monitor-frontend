"""Provides classes for the details component.

The classes in this module provide backing and control of the model for the Details.qml view.

"""


from PySide2.QtCore import Slot, QObject, Signal, QThread
import logging
from deepdiff import DeepDiff
from operator import itemgetter
import traceback
import time
from datetime import datetime, timezone
from threading import Thread, Lock
from itertools import groupby
import signal
from kraken_monitor.base import KrakenMonitorBridge


class DetailsBridgeDataHandler(QObject):
    """Handles incoming data from a connection.

    Contains methods that can build and return function objects to update the view model. The
    data handler should be moved to a QThread when instantiated using its inherited
    ``moveToThread`` method.

    Todo:
        * Setup control of sorting for the details view

    Args:
        shared_lock (Lock): Lock used for model update synchrononisation. Locked by the data
            handler when processing and unlocked by the bridge after update ops are performed.
        model (kraken_monitor.base.KrakenMonitorListModel): The model used in the ``DetailsBridge``.
        parent (QObject, optional): Parent of this object. Defaults to None.

    Attributes:
        log (logging.Logger): The logger object.
        model (kraken_monitor.base.KrakenMonitorListModel): The model used in the ``DetailsBridge``.
        signals (DetailsBridgeDataHandler.Signals): Signals used by the data handler.
        running (bool): Indicates whether the handler is running.
        paused (bool): Indicates whether the handler is paused.
        lock (Lock): Lock used for model update synchrononisation. Locked by the data handler when
            processing and unlocked by the bridge after update ops are performed.

    """

    class Signals(QObject):
        """Signals used by ``DetailsBridgeDataHandler``.

        Attributes:
            incoming_data (dict): Incoming digests passed through this signal.
            error (str): Handles error messages.
            model_updates (list): List of (func, [args]) update ops for use in the GUI thread.
        """
        incoming_data = Signal(dict)  # Received digest
        error = Signal(str)
        model_updates = Signal(list)  # Model update operations

    def __init__(self, shared_lock, model, parent=None):
        super().__init__(parent)
        self.log = logging.getLogger(__file__)
        self.model = model
        self.signals = DetailsBridgeDataHandler.Signals()
        self.current_data = {}
        self.running = False
        self.paused = False
        self.lock = Lock()  # Shared with main thread so that data update only goes through once
        self.sort_direction = "Ascending"  # Placeholder for sorting

    def get_index_ranges(self, data):
        """Helper method to group consecutive list elements.

        Args:
            data (list): List of integers to be grouped.

        Returns:
            list: List of tuples containing (start, end) of consecutive elements (inclusive).
        """
        ranges = []
        for k, g in groupby(enumerate(data), lambda x: x[0] - x[1]):
            group = (map(itemgetter(1), g))
            group = list(map(int, group))
            ranges.append((group[0], group[-1]))
        return ranges

    def update_model_ops(self, view_model, new_model):
        """Compares the current data in ``view_model`` with ``new_model`` and generates update ops.

        Used after the model generation step to compute the differences between existing and new
        models such that the view can be updated by the returned list of ops.

        Args:
            view_model (kraken_monitor.base.KrakenMonitorListModel): The ListModel to perform
                updates on.
            new_model (list): List of items that the view model will take as the new data.

        Returns:
            list: List of update operations as tuples of (func, [args]) that would transform the
                view model data into the new model.

        """

        ops = []
        old_model = view_model.model_data

        # Check item to see if anything has changed
        self.log.debug("Check for changes in model {}.".format(repr(view_model)))

        old_pids = [item["pid"] for item in old_model]
        new_pids = [item["pid"] for item in new_model]

        diff = DeepDiff(old_pids, new_pids, ignore_order=True)
        # # Deepdiff returns a dict item that is ordered by the values "root[0]", and we want the "0" only
        removals = [int(item[5:-1]) for item in diff["iterable_item_removed"].keys()] if "iterable_item_removed" in diff else []
        additions = [int(item[5:-1]) for item in diff["iterable_item_added"].keys()] if "iterable_item_added" in diff else []
        removals.sort()
        additions.sort()
        self.log.debug("Detected {} row removals ({}) and {} row additions ({}).".format(len(removals), removals, len(additions), additions))

        # Model updates for groups

        # Remove rows (in groups) from the right side
        removal_ranges = self.get_index_ranges(removals)
        for r in reversed(removal_ranges):
            index_start, index_end = r
            ops.append((view_model.remove_rows, (index_start, index_end - index_start + 1)))

        # Insert rows (in groups) from the left
        addition_ranges = self.get_index_ranges(additions)
        for r in addition_ranges:
            index_start, index_end = r
            items = new_model[index_start:index_end + 1]
            ops.append((view_model.insert_rows, (index_start, items)))

        self.log.debug("Checking property changes on untouched items.")

        # Check properties on untouched items to see if anything has changed
        additions_set = set(additions)
        removals_set = set(removals)
        new_items = [
            (i, item) for i, item in enumerate(new_model) if i not in additions_set
        ]
        old_items = [
            (i, item) for i, item in enumerate(old_model) if i not in removals_set
        ]

        for new, old in zip(new_items, old_items):
            new_index, new_item = new
            old_index, old_item = old

            self.log.debug("Checking item {}.".format(new_index))

            row_index = view_model.index(new_index)

            for key in view_model.role_names.keys():
                if key in new_item and not (new_item[key] == old_item[key]):
                    self.log.debug("{} property has changed, adding op.".format(key))
                    ops.append((view_model.set_data, (row_index, view_model.role_names[key], new_item[key])))

        return ops

    @Slot(dict)
    def run(self, digest):
        """Runs the processing pipeline to update the attached view.

        This method will reshape the digest data into a format that the model uses, then perform
        comparisons to generate the update operations needed to update the old model. A shared lock
        with the bridge object hosting the data handler is used to prevent multiple overlapping
        updates which may break the Qt view. This method should be connected to the incoming data
        signal. Will emit model update operations via signal.

        Args:
            digest (dict): The most recent digest.

        """
        if not self.paused:
            if self.lock.acquire(timeout=0):
                try:
                    self.log.debug("Running data handler method for change detection and update operations.")
                    t1 = time.perf_counter()
                    self.running = True

                    def sort_key(x):
                        return x["name"]

                    reverse = True if self.sort_direction == "Descending" else False
                    new_model = sorted(self.build_model(digest), key=sort_key, reverse=reverse)

                    update_ops = self.update_model_ops(self.model, new_model)
                    self.signals.model_updates.emit(update_ops)
                    t2 = time.perf_counter()
                    self.log.debug("Data handler completed in {} seconds.".format(t2 - t1))
                except Exception:
                    self.log.error("Exception occured in data handler method.")
                    self.signals.error.emit(traceback.format_exc())
                    self.lock.release()
                finally:
                    self.log.debug("Data handler finished.")
                    self.running = False
            else:
                self.log.info("Data handler overlapping, discarding.")

    def build_model(self, digest):
        """Builds a data model that is used in the attached view.

        The data model used for the ``DetailsListView`` is simply a list of dictionaries. This
        method sorts the digest items and strips out the first level of dictionary keys.

        Args:
            digest (dict): The most recent digest.

        Returns:
            list: The new data model.
        """

        self.log.debug("Building new model from digest data.")

        data = digest["data"]
        index = digest["index"]

        for key, item in data.items():
            for col, resolver in DetailsBridge.column_resolvers.items():
                if col in item:
                    item[col] = resolver(item[col])
            item["filename"] = key  # Add the filename entry as this data is removed

        model_data = [data[i] for i in index]

        self.log.debug("New model built.")
        return model_data

    def pause(self):
        """Pause processing of data.

        Possibly used when the attached view is not visible to save CPU time.

        """

        self.paused = True

    def resume(self):
        """Resume processing of data.

        """
        self.paused = False


class DetailsBridge(KrakenMonitorBridge):
    """Backs the ``Details.qml`` view.

    The bridge will provides methods that the view can run to process data, and provide event
    handlers to change the application behavior based on button clicks or other events in the view.
    The bridge also controls the lifecycle of its data handler.

    Args:
        data_connection (kraken_monitor.base.KrakenMonitorConnection): Data connection to use for
            retrieving digests and performing RPC calls.
        details_list_model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by
            the bridge.

    Attributes:
        column_resolvers (dict): Class attribute. Contains formatters for digest item properties so
            they can be pretty printed in the view.
        log (logging.Logger): The logger object.
        model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by the bridge.
        connection (kraken_monitor.base.KrakenMonitorConnection): The connection used by the bridge.
        thread (QThread): The thread used to run the data handler.
        data_handler (DetailsBridgeDataHandler): The data handler object.
        paused (bool): Indicates whether the bridge is paused.
        lock (Lock): Lock for processing data and applying changes to the model.

    """
    column_resolvers = {
        "cpu": lambda val: "{:02d}".format(int(val)),  # Two digit CPU utilisation display
        "memory": lambda val: "{:,} KiB".format(int(val / 1024)),  # Display memory in KiB
        "cpuAffinity": lambda val: ",".join(map(str, val)),  # Display cpu affinity as list
        "status": lambda val: "ALIVE" if val != "dead" and val != "zombie" else "DEAD",  # Group status into ALIVE or DEAD
        "creationTime": lambda val: "{}{:03.0f}".format(  # Format creation time as UTC with nanoseconds
            datetime.fromtimestamp(int(val) / 1e9, timezone.utc).strftime(
                "%Y-%m-%dT%H:%M:%S.%f"), int(val) % 1e3)
    }

    def __init__(self, data_connection, details_list_model):
        super().__init__()
        self.log = logging.getLogger(__file__)
        self.model = details_list_model
        self.connection = data_connection
        self.component = None
        self.thread = None
        self.data_handler = None
        self.paused = False
        self.lock = Lock()

    @Slot(list)
    def perform_model_update(self, ops):
        """Accepts a list of model update operations to perform.

        The model updates should have been deferred from the data handler.

        Args:
            ops (list): List of model update operations.

        """
        self.log.debug("Performing {} operations for model update.".format(len(ops)))
        if self.data_handler is not None and self.data_handler.lock.locked():
            try:
                t1 = time.perf_counter()
                for op in ops:
                    func, args = op
                    self.log.debug("Performing op {} with args {}.".format(func, args))
                    func(*args)
                t2 = time.perf_counter()
                self.log.debug("Model update performed in {} seconds.".format(t2 - t1))
            finally:
                # Ensure lock is released in any situation
                self.data_handler.lock.release()
        else:
            self.log.debug("Lock is not set, update not synchronised, discarding update.")

    @Slot(result=bool)
    def start(self, paused=False):
        """Starts the bridge and data handler.

        A ``QThread`` will be created to host the data handler operations.

        Args:
            paused (bool, optional): Start in paused mode. Defaults to False.

        Returns:
            bool: True if successful, False if not.

        """
        if self.data_handler is None:
            self.log.info("Starting Details Bridge data handler.")
            self.thread = QThread()
            self.data_handler = DetailsBridgeDataHandler(self.lock, self.model)
            if paused:
                self.pause()
            self.data_handler.moveToThread(self.thread)
            self.data_handler.signals.error.connect(self.error_handler)
            self.data_handler.signals.model_updates.connect(self.perform_model_update)
            self.connection.signals.process_manager_digest_received.connect(self.data_handler.signals.incoming_data)
            self.data_handler.signals.incoming_data.connect(self.data_handler.run)
            self.thread.start()
            self.log.info("Data handling thread created and awaiting data.")
            return True
        return False

    @Slot(result=bool)
    def stop(self, blocking=True):
        """Stops the bridge and data handler.

        Args:
            blocking (bool, optional): If True, will block the calling thread until everything
            stops. Otherwise runs asynchronously. Defaults to True.

        Returns:
            Any: True if successful, False if not. The stopping thread if not blocking.

        """
        def sequence():
            self.log.info("Shutting down data handler.")
            if self.data_handler is not None:
                self.data_handler.signals.model_updates.disconnect()
                self.data_handler.signals.incoming_data.disconnect()
                self.data_handler.signals.error.disconnect()
                self.log.info("Waiting for data handler to stop.")
                while self.data_handler.running:
                    time.sleep(0.5)
                    self.log.debug("Data handler status: {}.".format(self.data_handler.running))
            if self.thread is not None:
                self.thread.exit()
                self.thread.wait(5000)
                self.data_handler = None
                self.thread = None
            self.log.info("Data handler shutdown complete.")
            return True

        if not blocking:
            stop_thread = Thread(target=sequence)
            stop_thread.start()
            return stop_thread
        else:
            return sequence()
        return False

    @Slot(str)
    def error_handler(self, message):
        """Handles error messages from the data handler.

        Args:
            message (str): The error message
        """
        self.log.error("Error from data handler thread: {}.".format(message))
        self.stop(blocking=False)

    @Slot(result=bool)
    def pause(self):
        """Pauses the bridge.

        Will pause the data handler.

        Returns:
            bool: True if successful, False if not.
        """
        if self.data_handler is not None and not self.paused:
            self.log.debug("Pausing data handler.")
            self.data_handler.pause()
            self.paused = True
            return True
        return False

    @Slot(result=bool)
    def resume(self):
        """Resumes the bridge.

        Will resume the data handler.

        Returns:
            bool: True if successful, False if not.
        """
        if self.data_handler is not None and self.paused:
            self.log.debug("Resuming data handler.")
            self.data_handler.resume()
            self.paused = False
            return True
        return False

    @Slot(bool)
    def visible_changed_handler(self, visible):
        """Handler for QML visiblity change.

        Args:
            visible (bool): Current visiblity.
        """
        if visible:
            self.resume()
            try:
                self.connection.refresh_digest()
            except AssertionError:
                # Connection might not be ready
                pass
            self.model.refresh()
        else:
            self.pause()

    def csv_to_filenames(self, str_list):
        """Converts the csv string from QML to a proper list of filenames.

        The filenames are retrieved from model data.

        Args:
            str_list (str): The comma seperated list.

        Returns:
            list: List of filenames.
        """
        rows = []
        split = str_list.split(",")
        for row in split:
            try:
                index = int(row)
                # Check if the index is valid in the model data.
                if index >= 0 and index < len(self.model.model_data):
                    rows.append(self.model.model_data[index]["filename"])
            except ValueError:
                pass
        return rows

    @Slot(str, result=bool)
    def run_clicked_handler(self, csv):
        """Handles the run button clicks.

        Sets the control of the process to run.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_run(rows)
            return result

    @Slot(str, result=bool)
    def pause_clicked_handler(self, csv):
        """Handles the pause button clicks.

        Sets the control of the process to pause.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_pause(rows)
            return result
        return False

    @Slot(str, result=bool)
    def step_clicked_handler(self, csv):
        """Handles the step button clicks.

        Sets the control of the processes to step.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_step(rows)
            return result
        return False

    @Slot(str, result=bool)
    def stop_clicked_handler(self, csv):
        """Handles the stop button clicks.

        Sends the SIGTERM signal to the processes.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_exit(rows)
            return result
        return False

    @Slot(str, result=bool)
    def kill_clicked_handler(self, csv):
        """Handles the kill button clicks.

        Sends the SIGKILL signal to the processes.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_signal(9, rows)  # 9 is SIGKILL, Windows doesn't have the enum
            return result
        return False
