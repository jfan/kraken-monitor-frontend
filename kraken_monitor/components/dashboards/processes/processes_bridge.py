"""Provides classes for the processes component.

The classes in this module provide backing and control of the model for the Processes.qml view.

"""


from PySide2.QtCore import Slot, QObject, Signal, QThread
import logging
from collections import OrderedDict
from kraken_monitor.util import KrakenProcessCode
from .processes_list_model import ProcessesListModel
from kraken_monitor.base import KrakenMonitorBridge
from deepdiff import DeepDiff
from operator import itemgetter
import traceback
import time
from threading import Thread, Lock
from itertools import groupby
import signal


class ProcessesBridgeDataHandler(QObject):
    """Handles incoming data from a connection.

    Contains methods that can build and return function objects to update the view model. The
    data handler should be moved to a QThread when instantiated using its inherited
    ``moveToThread`` method.

    Args:
        shared_lock (Lock): Lock used for model update synchrononisation. Locked by the data
            handler when processing and unlocked by the bridge after update ops are performed.
        model (kraken_monitor.base.KrakenMonitorListModel): The model used in the
            ``ProcessesBridge``.
        parent (QObject, optional): Parent of this object. Defaults to None.

    Attributes:
        log (logging.Logger): The logger object.
        model (kraken_monitor.base.KrakenMonitorListModel): The model used in the
            ``ProcessesBridge``.
        signals (DetailsBridgeDataHandler.Signals): Signals used by the data handler.
        running (bool): Indicates whether the handler is running.
        paused (bool): Indicates whether the handler is paused.
        lock (Lock): Lock used for model update synchrononisation. Locked by the data handler when
            processing and unlocked by the bridge after update ops are performed.

    """

    class Signals(QObject):
        """Signals used by ``ProcessesBridgeDataHandler``.

        Attributes:
            incoming_data (dict): Incoming digests passed through this signal.
            error (str): Handles error messages.
            model_updates (list): List of (func, [args]) update ops for use in the GUI thread.
        """
        incoming_data = Signal(dict)
        error = Signal(str)
        model_updates = Signal(list)

    def __init__(self, shared_lock, model, parent=None):
        super().__init__(parent)
        self.log = logging.getLogger(__file__)
        self.model = model
        self.signals = ProcessesBridgeDataHandler.Signals()
        self.running = False
        self.paused = False
        self.sort_index = 0  # Placeholder for sorting
        self.sort_direction = "Ascending"  # Placeholder for sorting
        self.lock = shared_lock  # Shared with main thread so that data update only goes through once

    def get_sort_key_fn(self):
        """Generates a function that will be used in sorting as a key during model build.

        As the model data is not a flat array, the retrieval of the sort key is non-trivial and
        requires a lambda.

        Returns:
            Callable: The sorting key function.

        """
        index = self.sort_index
        if index == 0:
            return itemgetter("name")
        else:
            info_index = index - 1

            def key_fn(x):
                return itemgetter(info_index)(x["info"])
            return key_fn

    def get_index_ranges(self, data):
        ranges = []
        for k, g in groupby(enumerate(data), lambda x: x[0] - x[1]):
            group = (map(itemgetter(1), g))
            group = list(map(int, group))
            ranges.append((group[0], group[-1]))
        return ranges

    def update_model_ops(self, view_model, new_model):
        """Compares the current data in ``view_model`` with ``new_model`` and generates update ops.

        Used after the model generation step to compute the differences between existing and new
        models such that the view can be updated by the returned list of ops.

        Todo:
         * Find a new way to organise and/or update roles, current method is getting bloated.

        Args:
            view_model (kraken_monitor.base.KrakenMonitorListModel): The ListModel to perform
                updates on.
            new_model (list): List of items that the view model will take as the new data.

        Returns:
            list: List of update operations as tuples of (func, [args]) that would transform the
                view model data into the new model.

        """

        ops = []

        old_model = view_model.model_data

        # Check item to see if anything has changed
        self.log.debug("Check for top level changes in model {}.".format(repr(view_model)))

        old_names = [item["name"] for item in old_model]
        new_names = [item["name"] for item in new_model]

        diff = DeepDiff(old_names, new_names, ignore_order=True)
        # Deepdiff returns a dict item that is ordered by the values "root[0]", and we want the "0" only
        removals = [int(item[5:-1]) for item in diff["iterable_item_removed"].keys()] if "iterable_item_removed" in diff else []
        additions = [int(item[5:-1]) for item in diff["iterable_item_added"].keys()] if "iterable_item_added" in diff else []
        removals.sort()
        additions.sort()
        self.log.debug("Detected {} row removals ({}) and {} row additions ({}).".format(len(removals), removals, len(additions), additions))

        # Model updates for groups

        # Remove rows from the right side
        removal_ranges = self.get_index_ranges(removals)
        for r in reversed(removal_ranges):
            index_start, index_end = r
            ops.append((view_model.remove_rows, (index_start, index_end - index_start + 1)))

        # Insert rows from the left
        addition_ranges = self.get_index_ranges(additions)
        for r in addition_ranges:
            index_start, index_end = r
            items = new_model[index_start:index_end + 1]
            ops.append((view_model.insert_rows, (index_start, items)))

        self.log.debug("Checking property changes on untouched items.")

        # Check properties on untouched items to see if anything has changed
        additions_set = set(additions)
        removals_set = set(removals)
        new_items = [
            (i, item["name"], item["status"], item["control"], item["info"], item.get("children", None), item.get("childrenCount", None), item.get("isActive", False), item.get("isRunning", False))
            for i, item in enumerate(new_model) if i not in additions_set
        ]
        old_items = [
            (i, item["name"], item["status"], item["control"], item["info"], item.get("children", None), item.get("childrenCount", None), item.get("isActive", False), item.get("isRunning", False))
            for i, item in enumerate(old_model) if i not in removals_set
        ]

        # TODO: Find a better way to do this
        for new_item, old_item in zip(new_items, old_items):
            new_index, new_name, new_status, new_control, new_info, new_children_model, new_children_count, new_is_active, new_is_running = new_item
            old_index, old_name, old_status, old_control, old_info, old_children_model, old_children_count, old_is_active, old_is_running = old_item

            self.log.debug("Checking item {}.".format(new_index))
            row_index = view_model.index(new_index)

            if new_name != old_name:
                self.log.debug("Name property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["name"], new_name)))

            if new_status != old_status:
                self.log.debug("Status property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["status"], new_status)))

            if new_control != old_control:
                self.log.debug("Control property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["control"], new_control)))

            if new_info != old_info:  # This is elementwise natively
                self.log.debug("Info property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["info"], new_info)))

            if new_children_count != old_children_count:
                self.log.debug("Children count property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["childrenCount"], new_children_count)))

            if new_children_model is not None:
                if old_children_model is None:
                    self.log.debug("Children added to item, adding op.")
                    ops.append((view_model.set_data, (row_index, view_model.role_names["children"], new_children_model)))
                else:
                    self.log.debug("Performing change detection recursively on child models.")
                    for op in self.update_model_ops(old_children_model, new_children_model.model_data):
                        ops.append(op)

            if new_is_active != old_is_active:
                self.log.debug("IsActive property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["isActive"], new_is_active)))

            if new_is_running != old_is_running:
                self.log.debug("IsRunning property has changed, adding op.")
                ops.append((view_model.set_data, (row_index, view_model.role_names["isRunning"], new_is_running)))

        return ops

    @Slot(dict)
    def run(self, digest):
        """Runs the processing pipeline to update the attached view.

        This method will reshape the digest data into a format that the model uses, then perform
        comparisons to generate the update operations needed to update the old model. A shared lock
        with the bridge object hosting the data handler is used to prevent multiple overlapping
        updates which may break the Qt view. This method should be connected to the incoming data
        signal. Will emit model update operations via signal.

        Args:
            digest (dict): The most recent digest.

        """
        if not self.paused:
            if self.lock.acquire(timeout=0):
                try:
                    self.log.debug("Running data handler method for change detection and update operations.")
                    t1 = time.perf_counter()
                    self.running = True

                    sort_key = self.get_sort_key_fn()
                    reverse = True if self.sort_direction == "Descending" else False
                    new_model = sorted(self.build_model(digest), key=sort_key, reverse=reverse)

                    update_ops = self.update_model_ops(self.model, new_model)
                    self.signals.model_updates.emit(update_ops)
                    t2 = time.perf_counter()
                    self.log.debug("Data handler completed in {} seconds.".format(t2 - t1))
                except Exception:
                    self.log.error("Exception occured in data handler method.")
                    self.signals.error.emit(traceback.format_exc())
                    self.lock.release()
                finally:
                    self.log.debug("Data handler finished.")
                    self.running = False
            else:
                self.log.debug("Data handler overlapping, discarding.")

    def process_is_running(self, process_key, data):
        """Determines if a process is in a running state from digest data.

        Running means that it has a control value of RUNNING or STEP.

        Args:
            process_key (str): The key that refers to the digest item for the process.
            data (dict): The digest data.

        Returns:
            bool: True if running. False if not.

        """
        control = data[process_key][ProcessesBridge.column_map["Control"]["name"]]
        if control == KrakenProcessCode.Control.RUNNING.value or control == KrakenProcessCode.Control.STEP.value:
            return True
        return False

    def process_is_active(self, process_key, data):
        """Determines if a process is in an active state from digest data.

        Active means that the process is not dead and not a zombie.

        Args:
            process_key (str): The key that refers to the digest item for the process.
            data (dict): The digest data.

        Returns:
            bool: True if active. False if not.

        """
        status = data[process_key][ProcessesBridge.column_map["Status"]["name"]]
        return True if status != "dead" and status != "zombie" else False

    def build_model(self, digest):
        """
        Group processes by simillar names and build a data model. For example,
        we have MarlinConfigDaemon_*, MarlinBU_* etc. in one group.
        """

        self.log.debug("Building new model from digest data.")

        data = digest["data"]
        index = digest["index"]
        # columns = digest["columns"]

        # Get friendly names in parts
        name_parts = [(i, data[i]["name"].split("_")) for i in index]

        # Organise into process groups
        process_groups = OrderedDict()

        for i, parts in name_parts:
            key = parts[1] if len(parts) > 1 else parts[0]
            if key not in process_groups:
                process_groups[key] = []

            process_groups[key].append(i)

        self.log.debug("Collected {} process groups from {} processes.".format(len(process_groups), len(index)))

        # Build the new data model
        model_data = []
        column_map = ProcessesBridge.column_map
        column_keys = column_map.keys()
        for key, value in process_groups.items():
            # Get the child process properties
            children = []
            for child in value:
                info = []
                for col_name in column_keys:
                    resolver = column_map[col_name]["resolver"]
                    if resolver is None:
                        prop_name = column_map[col_name]["name"]
                        info.append(data[child][prop_name])
                    elif callable(resolver):
                        args = [data[child][prop_name] for prop_name in column_map[col_name]["args"]]
                        info.append(resolver(*args))
                children.append({
                    "filename": child,
                    "name": info[0],
                    "status": info[1],
                    "control": info[2],
                    "info": info[3:],
                    "isActive": self.process_is_active(child, data),
                    "isRunning": self.process_is_running(child, data)
                })

            # Get the group properties
            group_info = []
            for col_name in column_keys:
                col_meta = column_map[col_name]
                reduction_method = col_meta["reduction"]
                resolver = col_meta["resolver"]
                # Based on the reduction method of the column, display different things
                if reduction_method is None:
                    group_info.append(col_meta["default"])
                if reduction_method == "first":
                    # Take the first child process's property value
                    group_index = value[0]
                    if resolver is None:
                        prop_name = col_meta["name"]
                        group_info.append(data[group_index][prop_name])
                    elif callable(resolver):
                        args = [data[group_index][prop_name] for prop_name in col_meta["args"]]
                        group_info.append(resolver(*args))
                elif reduction_method == "sum":
                    # Sums the values held by this property in the group's child processes
                    prop_name = col_meta["name"]

                    if resolver is None:
                        intermediate = sum([data[child_index][prop_name] for child_index in value])
                    elif callable(resolver) and col_meta["args"] == [col_meta["name"]]:
                        intermediate = resolver(sum([data[child_index][prop_name] for child_index in value]))
                    elif callable(resolver):
                        intermediate = sum([resolver(data[child_index][prop_name]) for child_index in value])

                    group_info.append(intermediate)

            entry = {
                "name": key,
                "status": group_info[1],
                "control": group_info[2],
                "info": group_info[3:],
                "children": ProcessesListModel(data=children),
                "childrenCount": len(value),
                "isActive": children[0]["isActive"],
                "isRunning": children[0]["isRunning"]
            }

            model_data.append(entry)

        self.log.debug("New model built.")
        return model_data

    def pause(self):
        self.paused = True

    def resume(self):
        self.paused = False


class ProcessesBridge(KrakenMonitorBridge):
    """Backs the ``Processes.qml`` view.

    The bridge will provides methods that the view can run to process data, and provide event
    handlers to change the application behavior based on button clicks or other events in the view.
    The bridge also controls the lifecycle of its data handler.

    Args:
        data_connection (kraken_monitor.base.KrakenMonitorConnection): Data connection to use for
            retrieving digests and performing RPC calls.
        process_list_model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by
            the bridge.

    Attributes:
        column_map (dict): Class attribute. Contains mappings for the model columns to be mapped to
            the digest property values. Resolvers are used for pretty printing. Args provide
            arguments for the resolvers. Reduction specifies how process groups interpret its child
            process properties for its group display. Default specifies what is displayed where
            there is no data.
        log (logging.Logger): The logger object.
        model (kraken_monitor.base.KrakenMonitorListModel): The model controlled by the bridge.
        connection (kraken_monitor.base.KrakenMonitorConnection): The connection used by the bridge.
        thread (QThread): The thread used to run the data handler.
        data_handler (DetailsBridgeDataHandler): The data handler object.
        paused (bool): Indicates whether the bridge is paused.
        lock (Lock): Lock for processing data and applying changes to the model.

    """
    column_map = OrderedDict({
        "Name": {
            "name": "name",
            "resolver": None,
            "args": None,
            "reduction": "first",
            "default": "Unknown"
        },
        "Status": {
            "name": "status",
            "resolver": lambda val: "ALIVE" if val != "dead" and val != "zombie" else "DEAD",
            "args": ["status"],
            "reduction": "first",
            "default": ""
        },
        "Control": {
            "name": "control",
            "resolver": lambda val: KrakenProcessCode.Control(val).name.upper(),
            "args": ["control"],
            "reduction": "first",
            "default": ""
        },
        "CPU": {
            "name": "cpu",
            "resolver": lambda val: "{:.1f}%".format(val),
            "args": ["cpu"],
            "reduction": "sum",
            "default": ""
        },
        "Memory": {
            "name": "memory",
            "resolver": lambda val: "{:.1f} MiB".format(val / 1024 / 1024),
            "args": ["memory"],
            "reduction": "sum",
            "default": ""
        },
        "Loop count": {
            "name": "loopCount",
            "resolver": None,
            "args": None,
            "reduction": "first",
            "default": 0
        }
    })

    def __init__(self, data_connection, process_list_model):
        super().__init__()
        self.log = logging.getLogger(__file__)
        self.model = process_list_model
        self.connection = data_connection
        self.thread = None
        self.data_handler = None
        self.paused = False
        self.lock = Lock()

    @Slot(list)
    def perform_model_update(self, ops):
        """Accepts a list of model update operations to perform.

        The model updates should have been deferred from the data handler.

        Args:
            ops (list): List of model update operations.

        """
        self.log.debug("Performing {} operations for model update.".format(len(ops)))
        if self.data_handler is not None and self.data_handler.lock.locked():
            try:
                t1 = time.perf_counter()
                for op in ops:
                    func, args = op
                    self.log.debug("Performing op {} with args {}.".format(func, args))
                    func(*args)
                t2 = time.perf_counter()
                self.log.debug("Model update performed in {} seconds.".format(t2 - t1))
            finally:
                self.data_handler.lock.release()
        else:
            self.log.debug("Lock is not set, update not synchronised, discarding update.")

    @Slot(result=bool)
    def start(self, paused=False):
        """Starts the bridge and data handler.

        A ``QThread`` will be created to host the data handler operations.

        Args:
            paused (bool, optional): Start in paused mode. Defaults to False.

        Returns:
            bool: True if successful, False if not.

        """
        if self.data_handler is None:
            self.log.info("Starting Process Bridge data handler.")
            self.thread = QThread()
            self.data_handler = ProcessesBridgeDataHandler(self.lock, self.model)
            if paused:
                self.pause()
            self.data_handler.moveToThread(self.thread)
            self.data_handler.signals.error.connect(self.error_handler)
            self.data_handler.signals.model_updates.connect(self.perform_model_update)
            self.connection.signals.process_manager_digest_received.connect(self.data_handler.signals.incoming_data)
            self.data_handler.signals.incoming_data.connect(self.data_handler.run)
            self.thread.start()
            self.log.info("Data handling thread created and awaiting data.")
            return True
        return False

    @Slot(result=bool)
    def stop(self, blocking=True):
        """Stops the bridge and data handler.

        Args:
            blocking (bool, optional): If True, will block the calling thread until everything
            stops. Otherwise runs asynchronously. Defaults to True.

        Returns:
            Any: True if successful, False if not. The stopping thread if not blocking.

        """
        def sequence():
            self.log.info("Shutting down data handler.")
            if self.data_handler is not None:
                self.data_handler.signals.model_updates.disconnect()
                self.data_handler.signals.incoming_data.disconnect()
                self.data_handler.signals.error.disconnect()
                self.log.info("Waiting for data handler to stop.")
                while self.data_handler.running:
                    time.sleep(0.5)
                    self.log.debug("Data handler status: {}.".format(self.data_handler.running))
            if self.thread is not None:
                self.thread.exit()
                self.thread.wait(5000)
                self.data_handler = None
                self.thread = None
            self.log.info("Data handler shutdown complete.")
            return True

        if not blocking:
            stop_thread = Thread(target=sequence)
            stop_thread.start()
            return stop_thread
        else:
            return sequence()
        return False

        if not blocking:
            stop_thread = Thread(target=sequence)
            stop_thread.start()
        else:
            sequence()

    @Slot(str)
    def error_handler(self, message):
        """Handles error messages from the data handler.

        Args:
            message (str): The error message
        """
        self.log.error("Error from data handler thread: {}.".format(message))
        self.stop(blocking=False)

    @Slot(result=bool)
    def pause(self):
        """Pauses the bridge.

        Will pause the data handler.

        Returns:
            bool: True if successful, False if not.
        """
        if self.data_handler is not None and not self.paused:
            self.log.debug("Pausing data handler.")
            self.data_handler.pause()
            self.paused = True
            return True
        return False

    @Slot(result=bool)
    def resume(self):
        """Resumes the bridge.

        Will resume the data handler.

        Returns:
            bool: True if successful, False if not.
        """
        if self.data_handler is not None and self.paused:
            self.log.debug("Resuming data handler.")
            self.data_handler.resume()
            self.paused = False
            return True
        return False

    @Slot(bool)
    def visible_changed_handler(self, visible):
        """Handler for QML visiblity change.

        Args:
            visible (bool): Current visiblity.
        """
        if visible:
            self.resume()
            try:
                self.connection.refresh_digest()
            except AssertionError:
                # Connection might not be ready
                pass
            self.model.refresh()
        else:
            self.pause()

    def csv_to_filenames(self, str_list, include_config=True, only_alive=False):
        """Converts the csv string from QML to a proper list of filenames.

        The filenames are retrieved from model data.

        Args:
            str_list (str): The comma seperated list.
            include_config (bool, optional): If True, include config process filenames in return
                value. Defaults to True.
            only_alive (bool, optional): If True, only select process groups that are alive.
                Defaults to False.

        Returns:
            list: List of filenames.
        """
        rows = []
        split = str_list.split(",")
        for row in split:
            try:
                index = int(row)
                if index >= 0 and index < len(self.model.model_data) and (not only_alive or self.model.model_data[index]["isActive"]):
                    for child in self.model.model_data[index]["children"].model_data:
                        # Gather all child processes but don't include the ConfigDaemon usually
                        if include_config or child["name"].split("_")[0] != "MarlinConfigDaemon":
                            rows.append(child["filename"])
            except ValueError:
                pass
        return rows

    @Slot(str, result=bool)
    def run_clicked_handler(self, csv):
        """Handles the run button clicks.

        Sets the control of the process to run.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv, include_config=False, only_alive=True)
        if len(rows) > 0:
            result = self.connection.process_run(rows)
            return result

    @Slot(str, result=bool)
    def pause_clicked_handler(self, csv):
        """Handles the pause button clicks.

        Sets the control of the process to pause.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv, include_config=False, only_alive=True)
        if len(rows) > 0:
            result = self.connection.process_pause(rows)
            return result
        return False

    @Slot(str, result=bool)
    def step_clicked_handler(self, csv):
        """Handles the step button clicks.

        Sets the control of the processes to step.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv, include_config=False, only_alive=True)
        if len(rows) > 0:
            result = self.connection.process_step(rows)
            return result
        return False

    @Slot(str, result=bool)
    def stop_clicked_handler(self, csv):
        """Handles the stop button clicks.

        Sends the SIGTERM signal to the processes.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_exit(rows)
            return result
        return False

    @Slot(str, result=bool)
    def kill_clicked_handler(self, csv):
        """Handles the kill button clicks.

        Sends the SIGKILL signal to the processes.

        Args:
            csv (str): The comma selerated list.

        Returns:
            bool: True if successful, False if not.

        """
        rows = self.csv_to_filenames(csv)
        if len(rows) > 0:
            result = self.connection.process_signal(9, rows)  # 9 is SIGKILL, Windows doesn't have the enum
            return result
        return False
