/*** Processes View

Intended to be the user friendly view. This view groups Kraken processes into their respective 
groups, with most likely a business unit and config process.

***/


import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import "delegates"
import "../../../components"

Page {
    // List of columns should match the column mapping in processesBridge
    property var listColumnHeaders: ["Name", "Status", "Control", "CPU", "Memory", "Loop count"]
    property var listColumnWidths: [260, 90, 90, 90, 90, 90]
    property var listColumnTextAlignments: [Text.AlignLeft, Text.AlignLeft, Text.AlignLeft, Text.AlignRight, Text.AlignRight, Text.AlignRight]
    property int processGroupRowHeight: 40 // The expand button is also this size in width and height
    property int scrollBarSize: 20

    objectName: "Processes"
    id: pageProcesses
    height: parent.height
    width: parent.width

    // Run on visiblity change
    function visibleChangedHandler() {
        processesBridge.visible_changed_handler(visible);
        if (visible) {
            // Force focus on the list view first
            processGroupList.forceActiveFocus();
        }
        else {
            processGroupList.currentIndex = -1;
        }
    }

    Component.onCompleted: {
        visibleChangedHandler();
    }

    onVisibleChanged: {
        visibleChangedHandler();
    }

    Item {
        anchors.fill: parent
        Flickable {
            id: outerScrollView
            clip: true
            ScrollBar.horizontal: outerContentScroll
            flickableDirection: Flickable.HorizontalFlick
            boundsBehavior: Flickable.StopAtBounds
            anchors.top: parent.top
            anchors.bottom: outerContentScroll.top
            anchors.left: parent.left
            anchors.right: parent.right
            contentWidth: processGroupList.width
            RowLayout {
                id: columnHeaders
                height: 60
                spacing: 0
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right

                Rectangle {
                    Layout.fillHeight: true
                    Layout.preferredWidth: listColumnWidths[0] + processGroupRowHeight
                    color: "transparent"
                    Text {
                        clip: true
                        anchors.fill: parent
                        leftPadding: 10
                        topPadding: 5
                        bottomPadding: 5
                        rightPadding: 10
                        text: listColumnHeaders[0]
                        verticalAlignment: Text.AlignBottom
                        horizontalAlignment: listColumnTextAlignments[0]
                        color: Material.accent
                    }
                    CustomBorder {
                        bottomBorderSize: 1
                        rightBorderSize: 1
                        borderColor: Material.color(Material.Grey, Material.Shade500)
                    }
                }
                Repeater {
                    model: listColumnHeaders.length - 1
                    delegate: Rectangle {
                        Layout.fillHeight: true
                        Layout.preferredWidth: listColumnWidths[index + 1]
                        color: "transparent"
                        Text {
                            clip: true
                            anchors.fill: parent
                            leftPadding: 10
                            topPadding: 5
                            bottomPadding: 5
                            rightPadding: 10
                            text: listColumnHeaders[index + 1]
                            verticalAlignment: Text.AlignBottom
                            horizontalAlignment: listColumnTextAlignments[index + 1]
                            color: Material.accent
                        }
                        CustomBorder {
                            bottomBorderSize: 1
                            rightBorderSize: 1
                            borderColor: Material.color(Material.Grey, Material.Shade500)
                        }
                    }
                }

                // Placeholder item for layout
                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    CustomBorder {
                        bottomBorderSize: 1
                        borderColor: Material.color(Material.Grey, Material.Shade500)
                    }
                }
            }

            ListView {
                property int calculatedWidth: listColumnWidths.reduce((a, b) => a + b, 0) + processGroupRowHeight + scrollBarSize
                id: processGroupList
                anchors.top: columnHeaders.bottom
                width: calculatedWidth < outerScrollView.width ? outerScrollView.width : calculatedWidth
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                clip: true
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds
                ScrollBar.vertical: processGroupScroll
                model: processesListModel
                keyNavigationEnabled: true
                currentIndex: -1
                enabled: dataConnection.connection_state === 0

                delegate: Item {
                    property bool hovered: processGroupMouseArea.containsMouse || topProcess.buttonHover
                    property bool selected: processGroupList.currentIndex === index
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: processGroupLayout.implicitHeight

                    function clickHandler() {
                        if (isActive) {
                            processGroupList.currentIndex = index;
                        }
                        processGroupList.forceActiveFocus();
                    }

                    Rectangle {
                        z: -2
                        anchors.fill: parent
                        color: hovered && isActive ? Material.shade(Material.accent, Material.Shade100) : "transparent"
                    }

                    Rectangle {
                        z: -1
                        anchors.fill: parent
                        color: selected ? Material.shade(Material.accent, Material.Shade200) : "transparent"
                    }

                    Rectangle {
                        z: 1
                        anchors.fill: parent
                        color: isActive && dataConnection.connection_state === 0 ? "transparent" : colorAlpha(Material.color(Material.Grey), 0.65)
                    }

                    MouseArea {
                        id: processGroupMouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: {
                            clickHandler();
                        }
                        onDoubleClicked: {
                            topProcess.expandState = !topProcess.expandState
                        }
                    }

                    RowLayout {
                        id: processGroupLayout
                        anchors.fill: parent
                        ColumnLayout {
                            Layout.fillWidth: true
                            id: processListBody
                            spacing: 0

                            ProcessDelegate {
                                id: topProcess
                                Layout.fillWidth: true
                                height: processGroupRowHeight
                                isGroup: true
                                columnWidths: listColumnWidths
                                columnAlignments: listColumnTextAlignments
                                processName: name
                                processStatus: status
                                processControl: control
                                processInfo: info
                                processRunning: isRunning
                                processActive: isActive
                                childrenCount: modelData.childrenCount
                                onExpandStateChanged: {
                                    // Fixes bug for clicking on list item and not focusing
                                    clickHandler();
                                }
                            }

                            ListView {
                                interactive: false
                                id: subprocessList
                                implicitHeight: contentHeight
                                Layout.fillWidth: true
                                model: modelData.children
                                visible: topProcess.expandState
                                highlight: Rectangle { color: "blue";}
                                highlightFollowsCurrentItem: true
                                keyNavigationEnabled: true
                                currentIndex: -1
                                highlightMoveDuration: 0

                                delegate: Item {
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    height: process.height

                                    // TODO: Perhaps allow sublist selections
//                                    MouseArea {
//                                        anchors.fill: parent
//                                        onClicked: {
//                                            subprocessList.currentIndex = index;
//                                            subprocessList.forceActiveFocus();
//                                        }
//                                    }
                                    ProcessDelegate {
                                        id: process
                                        anchors.fill: parent
                                        height: 30
                                        isGroup: false
                                        columnWidths: listColumnWidths
                                        columnAlignments: listColumnTextAlignments
                                        processName: name
                                        processInfo: info
                                        processStatus: status
                                        processControl: control
                                        processRunning: isRunning
                                        processActive: isActive
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        ScrollBar {
            id: processGroupScroll
            width: scrollBarSize
            height: processGroupList.height
            background: Rectangle {
                color: Material.color(Material.Grey, Material.Shade300)
            }
            anchors.right: parent.right
            anchors.bottom: bottomBar.top
            anchors.bottomMargin: scrollBarSize
            policy: ScrollBar.AlwaysOn
            focusPolicy: Qt.NoFocus
        }

        ScrollBar {
            id: outerContentScroll
            height: scrollBarSize
            anchors.left: parent.left
            anchors.right: processGroupScroll.left
            anchors.bottom:bottomBar.top
            policy: ScrollBar.AlwaysOn
            background: Rectangle {
                color: Material.color(Material.Grey, Material.Shade300)
            }
            focusPolicy: Qt.NoFocus
        }

        Item {
            id: bottomBar
            height: 60
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            RowLayout {
                anchors.fill: parent
                anchors.margins: 10
                spacing: 10

                Item {
                    Layout.fillWidth: true
                }

                Button {
                    id: runButton
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.preferredWidth: 100
                    Layout.preferredHeight: 40
                    enabled: dataConnection.connection_state === 0
                    text: qsTr("Run")
                    onClicked: {
                        processesBridge.run_clicked_handler(processGroupList.currentIndex);
                    }
                }

                Button {
                    id: stepButton
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.preferredWidth: 100
                    Layout.preferredHeight: 40
                    enabled: dataConnection.connection_state === 0
                    text: qsTr("Step")
                    onClicked: {
                        processesBridge.step_clicked_handler(processGroupList.currentIndex);
                    }
                }

                Button {
                    id: pauseButton
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.preferredWidth: 100
                    Layout.preferredHeight: 40
                    enabled: dataConnection.connection_state === 0
                    text: qsTr("Pause")
                    onClicked: {
                        processesBridge.pause_clicked_handler(processGroupList.currentIndex);
                    }
                }

                Button {
                    id: stopButton
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.preferredWidth: 100
                    Layout.preferredHeight: 40
                    enabled: dataConnection.connection_state === 0
                    text: qsTr("Stop")
                    onClicked: {
                        processesBridge.stop_clicked_handler(processGroupList.currentIndex);
                    }
                }
                Button {
                    id: killButton
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.preferredWidth: 100
                    Layout.preferredHeight: 40
                    enabled: dataConnection.connection_state === 0
                    text: qsTr("Kill")
                    onClicked: {
                        processesBridge.kill_clicked_handler(processGroupList.currentIndex);
                    }
                }
            }

            CustomBorder {
                topBorderSize: 1
                borderColor: Material.color(Material.Grey)
            }
        }
    }
}
