"""Provides the model used in the QML Processes view.

"""

from kraken_monitor.base import KrakenMonitorListModel

class ProcessesListModel(KrakenMonitorListModel):
    """List of items representing process groups.

    The main data structure is a list of dictionaries. Each dictionary contains the roles as keys.
    Each dictionary represents a group of processes running on the server. Top level dictionaries 
    contains the keys/roles ``children`` and ``childrenCount`` to indicate how many processes are 
    grouped on that level. Children values contain another nested ``ProcessListModel`` containing 
    their processes. The maximum nesting depth is 2 (the group and then the process).

    Args:
        data (list, optional): The list of dictionaries used as data. Defaults to [].
        parent (PySide2.QtCore.QObject, optional) The parent of this object. Defaults to None.

    """
    def __init__(self, data=[], parent=None):
        super().__init__(data, parent)

    def define_roles(self):
        """See ``kraken_monitor.base.KrakenMonitorListModel.define_roles``.
        
        """
        return dict((KrakenMonitorListModel.ModelData + 1 + i, role) for i, role in enumerate([
            b"name", 
            b"status",
            b"control",
            b"info", 
            b"children", 
            b"childrenCount",
            b"isActive",
            b"isRunning"
        ]))
