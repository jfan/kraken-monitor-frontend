"""User friendly Kraken process properties.

The processes module contains the bridge, model and QML view for displaying all the immediately 
relevant properties. The processes are grouped into process groups, with a business unit and config 
unit together. Values are pretty printed where possible.

Todo:
 * Implement sorting on the columns.
 * Implement column swapping/moving.
 * Implement friendly views for more process properties.
 * Implement selecting properties for view.
 * Find out how to reduce/combine isActive and isRunning so that the group disabled state better reflects all children.

"""

from .processes_bridge import ProcessesBridge
from .processes_list_model import ProcessesListModel
