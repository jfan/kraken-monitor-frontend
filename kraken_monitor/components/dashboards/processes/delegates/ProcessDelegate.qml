/*** Delegate for process list.

Displays the process information in columns from name and info. Will provide an expand button if 
the item is marked as ``isGroup``.

***/


import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12
import "../../../../components"

Rectangle {
    id: root
    property bool isGroup: true
    property bool expandState: false
    property var columnWidths: [150]
    property var columnAlignments: [Text.AlignLeft]
    property var processInfo: []
    property string processName: qsTr("Reference error")
    property string processStatus: qsTr("Reference error")
    property string processControl: qsTr("Reference error")
    property bool processRunning: true
    property bool processActive: true
    property int childrenCount: 0
    property bool buttonHover: false

    color: "transparent"

    RowLayout {
        id: groupRow
        anchors.fill: parent
        spacing: 0
        Button {
            Layout.fillHeight: true
            Layout.preferredWidth: 40
            background: Rectangle {
                color: "transparent"
            }
            onHoveredChanged: buttonHover = hovered
            hoverEnabled: true
            visible: isGroup
            onClicked: {
                expandState = !expandState;
            }
            icon {
                name: expandState ? "go-down" : "go-next"
                source: expandState ? "../assets/chevron-down.svg" : "../assets/chevron-right.svg"
                width: expandState ? 12 : 8
                height: 12
                color: hovered ? Material.accent : Material.foreground
            }
        }

        Rectangle {
            Layout.leftMargin: isGroup ? 0 : 40
            Layout.fillHeight: true
            Layout.preferredWidth: columnWidths[0]
            color: "transparent"
            Text {
                clip: true
                anchors.leftMargin: isGroup ? 0 : 10
                leftPadding: 10
                topPadding: 5
                bottomPadding: 5
                rightPadding: 10
                anchors.fill: parent
                text: isGroup ? processName + " (" + childrenCount.toString() + ")" : processName
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: columnAlignments[0]
            }
            CustomBorder {
                rightBorderSize: 1
                borderColor: colorAlpha(Material.accent, 0.2)
            }
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.preferredWidth: columnWidths === undefined ? 130 : columnWidths[1]
            color: processActive ? colorAlpha(Material.color(Material.Green), 0.2) : colorAlpha(Material.color(Material.Red), 0.2)

            Text {
                clip: true
                anchors.fill: parent
                leftPadding: 10
                topPadding: 5
                bottomPadding: 5
                rightPadding: 10
                text: processStatus
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: columnAlignments[1]
            }
            CustomBorder {
                rightBorderSize: 1
                borderColor: colorAlpha(Material.accent, 0.2)
            }
        }

        Rectangle {
            Layout.fillHeight: true
            Layout.preferredWidth: columnWidths === undefined ? 130 : columnWidths[2]
            color: processRunning ? colorAlpha(Material.color(Material.Green), 0.2) : colorAlpha(Material.color(Material.Amber), 0.2)

            Text {
                clip: true
                anchors.fill: parent
                leftPadding: 10
                topPadding: 5
                bottomPadding: 5
                rightPadding: 10
                text: processControl
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: columnAlignments[2]
            }
            CustomBorder {
                rightBorderSize: 1
                borderColor: colorAlpha(Material.accent, 0.2)
            }
        }

        Repeater {
            model: processInfo.length
            delegate: Rectangle {
                Layout.fillHeight: true
                Layout.preferredWidth: columnWidths === undefined ? 130 : columnWidths[index + 3]
                color: colorAlpha(Material.accent, 0.2)

                Text {
                    clip: true
                    anchors.fill: parent
                    leftPadding: 10
                    topPadding: 5
                    bottomPadding: 5
                    rightPadding: 10
                    text: processInfo[index]
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: columnAlignments[index + 3]
                }
                CustomBorder {
                    rightBorderSize: 1
                    borderColor: colorAlpha(Material.accent, 0.2)
                }
            }
        }

        // Placeholder item for layout
        Rectangle {
            color: colorAlpha(Material.accent, 0.2)
            Layout.fillWidth: true
            Layout.fillHeight: true
            CustomBorder {
                rightBorderSize: 1
                borderColor: colorAlpha(Material.accent, 0.2)
            }
        }
    }

}


/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
