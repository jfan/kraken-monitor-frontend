/*** Put this anywhere for a border around it.

***/


import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Item {
    property int leftBorderSize: 0
    property int rightBorderSize: 0
    property int topBorderSize: 0
    property int bottomBorderSize: 0
    property string borderColor: "black"
    anchors.fill: parent

    Rectangle {
        id: bottomBorder
        height: bottomBorderSize
        color: borderColor
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
    Rectangle {
        id: topBorder
        height: topBorderSize
        color: borderColor
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }
    Rectangle {
        id: leftBorder
        width: leftBorderSize
        color: borderColor
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.top: parent.top
    }
    Rectangle {
        id: rightBorder
        width: rightBorderSize
        color: borderColor
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.right: parent.right
    }
}
