"""Provides classes used for connection to a remote Kraken server.

The connection objects perform the task of fetching data from a remote server running Kraken
server. The connection is established through ``aiomas``, utilising ``asyncio`` for asynchronous
RPC calls.

"""

import aiomas
import asyncio
import socket
import logging
from threading import Lock, Thread
from PySide2.QtCore import QThread, QObject, Signal, Slot, Property
import time
import traceback
from enum import Enum
from kraken_monitor.base import KrakenMonitorConnection
from copy import deepcopy


class ConnectionState(Enum):
    """Enum of connection states available.

    """
    CONNECTED = 0
    DISCONNECTED = 1
    CONNECTING = 2


class KrakenServerAgent(Enum):
    """Gives names of agents available in ``KrakenServer``.

    """
    PROCESS_MANAGER = 0
    SHARE_MANAGER = 1


class KrakenServerRPCAgent(aiomas.Agent):
    """Establishes connections to a remote agent.

    The RPC agent is responsible for establishing a connection to the given container with the
    given server parameters. RPC calls can be made with ``make_call``. The container and agent uses
    an ``asyncio`` event loop, so an event loop must be started on the thread this object is
    running on.

    Args:
        container (aiomas.Container): Container used to house the agent.
        remote_host (str): Hostname of the remote container.
        remote_port (int): Port of the remote container.
        remote_protocol (str, optional): Protocol for the remote container. Defaults to "tcp".
        remote_id (int, optional): Id of the remote agent. Defaults to 0.

    Attributes:
        log (logging.Logger): Logger object.
        remote_host (str): Hostname of the remote container.
        remote_port (int): Port of the remote container.
        remote_protocol (str): Protocol for the remote agent. Defaults to "tcp".
        agents (list): Contains agent objects representing the remote agents when connected.

    """
    def __init__(self, container, remote_host, remote_port, remote_protocol="tcp"):
        super().__init__(container)
        self.log = logging.getLogger(__file__)
        self.remote_host = remote_host
        self.remote_port = remote_port
        self.remote_protocol = remote_protocol
        self.agents = [None, None]

    def check_connection(self, timeout=3):
        s = None
        connected = False
        try:
            self.log.debug("Testing connection to {}:{}.".format(self.remote_host, self.remote_port))
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(timeout)
            s.connect((self.remote_host, self.remote_port))
            connected = True
        except (socket.timeout, ConnectionError):
            return False
        finally:
            if s is not None and connected:
                connected = False
                s.shutdown(socket.SHUT_RDWR)
                s.close()
        return True

    async def connect(self, timeout=3):
        """Asynchronously establish connection to the remote agents.

        Args:
            timeout (int, optional): Amount of time in seconds to try to connect. Defaults to 5.

        Returns:
            bool: True if successful, False if not.

        """
        try:
            base_address = "{}://{}:{}/".format(self.remote_protocol, self.remote_host, self.remote_port)
            process_manager_agent_address = base_address + "0"
            share_manager_agent_address = base_address + "1"
            self.log.info("Agent connecting to {}.".format(base_address))
            try:
                if not self.check_connection(timeout):
                    raise ConnectionRefusedError("Cannot connect to the specified socket")
                self.agents[0] = await self.container.connect(process_manager_agent_address, timeout)
                self.agents[1] = await self.container.connect(share_manager_agent_address, timeout)
            except (ConnectionError, FileNotFoundError, TimeoutError) as error:
                self.log.info("Connection failed, {}.".format(error))
                return False
            else:
                self.log.info("Connection established.")
            return True
        except asyncio.CancelledError:
            self.log.info("Connection canceled.")
            return False

    async def make_call(self, agent, func_name, args=[], kwargs={}):
        """Asynchronously call function exposed on the remote agent.

        Args:
            agent (KrakenServerAgent): The agent to call.
            func_name (str): Name of the exposed function to call.
            args (list, optional): Positional arguments to pass. Defaults to [].
            kwargs (dict, optional): Named arguments to pass. Defaults to {}.

        Returns:
            Any: The result of the function call. None if not connected.

        Raises:
            aiomas.RemoteException: Exception occured on remote agent during RPC.

        """
        try:
            t1 = time.perf_counter()
            agent = self.agents[agent.value]
            self.log.debug("Calling {} on remote agent {}.".format(func_name, agent))
            func = getattr(agent, func_name)
            data = await func(*args, **kwargs)
            t2 = time.perf_counter()
            self.log.debug("Call returned in {} seconds.".format(t2 - t1))
            return data
        except asyncio.CancelledError:
            self.log.info("Call cancelled.")


class KrakenServerWorker(QObject):
    """Worker used to start a ``KrakenServerRPCAgent`` and poll for data.

    When started, the worker will establish an connection between the local and remote agent before
    polling per ``data_interval`` for data digests from the Kraken server. The worker will manage
    the lifetime of the agent and the container it is in. The worker has a ``digest_received``
    signal that bridges can connect to for their event loop. The worker should be moved to a
    QThread when instantiated using its inherited ``moveToThread`` method. The QThread start signal
    should be connected to the ``run`` method.

    Args:
        data_signals (QObject): Object with the data signals ``process_manager_data_received`` and
            ``share_manager_data_received``.
        remote_host (str): Hostname of the remote container.
        remote_port (int): Port of the remote container.
        remote_protocol (str, optional): Protocol for the remote container. Defaults to "tcp".
        remote_id (int, optional): Id of the remote agent. Defaults to 0.
        data_interval (int, optional): Polling interval to get digest. Defaults to 1.
        blosc (bool, optional): Use blosc compression. Must be same as server. Defaults to True.
        parent (QObject, optional): Parent of this QObject. Defaults to None.

    Attributes:
        log (logging.Logger): Logger object.
        remote_host (str): Hostname of the remote container.
        remote_port (int): Port of the remote container.
        remote_protocol (str): Protocol for the remote container.
        remote_id (int): Id of the remote agent.
        data_interval: Polling interval to get digest.
        blosc (bool): Indicates whether blosc compression is used. Only used when starting
            container.
        request_locks (dict): Dictionary of locks. Keeps requests from overlapping by locking per
            digest request.
        connect_task (asyncio.Task): The connection task started by the worker.
        request_loop_task (asyncio.Task): The request loop task started by the worker.
        connection_state (ConnectionState): Indicates whether we are connected or not.
        signals (KrakenServerWorker.Signals): Contains the signals available from the worker.
        data_signals (QObject): Object with the data signals ``process_manager_data_received`` and
            ``share_manager_data_received``.
        loop (asyncio.loop): Contains the asyncio event loop created for the worker QThread.
        share_manager_digest_cache (dict): The most recent digest from the share agent.
        process_manager_digest_cache (dict): The most recent digest from the process agent.

    """
    class Signals(QObject):
        """Signals used in ``KrakenServerWorker``.

        Attributes:
            error (str): Errors occured in the worker thread.
            connection_state_changed (int): Signal emitted when running state changes.

        """
        error = Signal(str)  # Connect this to handle errors coming from a QThread.
        connection_state_changed = Signal(int)

    def __init__(self, data_signals, remote_host, remote_port, remote_protocol="tcp", data_interval=1, blosc=True):
        super().__init__()
        self.log = logging.getLogger(__file__)
        self.remote_host = remote_host
        self.remote_port = remote_port
        self.remote_protocol = remote_protocol
        self.data_interval = data_interval
        self.container = None
        self.agent = None
        self.blosc = blosc
        self.request_locks = {
            KrakenServerAgent.PROCESS_MANAGER: Lock(),
            KrakenServerAgent.SHARE_MANAGER: Lock()
        }
        self.connect_task = None
        self.request_loop_task = None
        self.signals = KrakenServerWorker.Signals()
        self.set_connection_state(ConnectionState.DISCONNECTED)
        self.data_signals = data_signals
        self.loop = None
        self.process_manager_digest_cache = None
        self.share_manager_digest_cache = None

    def set_connection_state(self, state):
        """Helper function to set the connection state and emit on signal.
        
        Args:
            state (ConnectionState): The new connection state.
        """
        self.connection_state = state
        self.signals.connection_state_changed.emit(self.connection_state.value)


    @Slot()
    def run(self):
        """Starts the worker.

        The worker will start up an ``asyncio`` event loop on the thread it is running on unless
        one already exists. This loop will be used to startup the ``aiomas`` agent and container to
        connect to the remote Kraken server. A request loop will be started to poll for digest data.
        This method will block the current thread with the ``asyncio`` event loop.

        If ``stop`` is called, the method will cleanup after itself and shut down the event loop
        along with ``aiomas`` objects.

        """
        try:
            if self.connection_state == ConnectionState.DISCONNECTED:
                self.set_connection_state(ConnectionState.CONNECTING)
                try:
                    self.loop = asyncio.get_event_loop()
                except RuntimeError:
                    self.loop = asyncio.new_event_loop()

                asyncio.set_event_loop(self.loop)

                self.log.info("Starting container.")
                self.container = aiomas.Container.create(
                    aiomas.local_queue.get_queue("default"), codec=aiomas.MsgPack if not self.blosc else aiomas.MsgPackBlosc)
                self.log.info("Container started.")

                try:
                    self.agent = KrakenServerRPCAgent(self.container, self.remote_host, self.remote_port, self.remote_protocol)
                    self.connect_task = asyncio.ensure_future(self.agent.connect())
                    connection_result = self.loop.run_until_complete(self.connect_task)
                    self.connect_task = None
                    self.log.debug("Connection result is {}".format(connection_result))

                    if connection_result:
                        self.set_connection_state(ConnectionState.CONNECTED)
                        self.request_loop_task = asyncio.ensure_future(self.digest_loop())
                        self.loop.run_until_complete(self.request_loop_task)
                    else:
                        self.log.error("Connection could not be established to remote container.")
                except ConnectionResetError:
                    # Server closed
                    pass
                finally:
                    self.log.info("Shutting down container.")
                    self.set_connection_state(ConnectionState.DISCONNECTED)
                    self.loop.run_until_complete(self.container.shutdown(as_coro=True))
                    self.container = None
                    self.agent = None
        except Exception:
            self.signals.error.emit(traceback.format_exc())
            self.set_connection_state(ConnectionState.DISCONNECTED)
            self.container = None
            self.agent = None

    def call_async(self, agent, func_name, args=[], kwargs={}, callback=None):
        """Peform a function call on the remote agent.

        This method will create an asynchronous task to be performed by the current event loop.

        Args:
            agent (KrakenServerAgent): The agent to request from.
            func_name (str): Name of the exposed function to call.
            args (list, optional): Positional arguments to pass. Defaults to [].
            kwargs (dict, optional): Named arguments to pass. Defaults to {}.
            callback (Callable, optional): Callback to handle the data. Defaults to None.

        Returns:
            Any: The result of the function call.

        """
        try:
            if self.connection_state == ConnectionState.CONNECTED:
                async def make_call_with_callback():
                    try:
                        data = await self.agent.make_call(agent, func_name, args, kwargs)
                        if callback is not None:
                            callback(data)
                    except Exception:
                        self.signals.error.emit(traceback.format_exc())
                asyncio.ensure_future(make_call_with_callback(), loop=self.loop)
                return True
        except Exception:
            self.signals.error.emit(traceback.format_exc())
        return False

    def refresh_digest(self):
        """Manually trigger the digest request loop to run with cached data.

        This is used between data intervals to ensure that connected components receive a digest
        update on initialisation or other change.

        Returns:
            bool: True if successful, False if not.

        """
        try:
            if self.connection_state == ConnectionState.CONNECTED:
                async def signal_emit(signal, object):
                    signal.emit(deepcopy(object))

                if self.share_manager_digest_cache is not None:
                    asyncio.ensure_future(signal_emit(self.data_signals.share_manager_digest_received, self.share_manager_digest_cache), loop=self.loop)
                if self.process_manager_digest_cache is not None:
                    asyncio.ensure_future(signal_emit(self.data_signals.process_manager_digest_received, self.process_manager_digest_cache), loop=self.loop)
                return True
        except Exception:
            self.signals.error.emit(traceback.format_exc())
        return False

    def stop(self):
        """Stops the worker.

        This method will cancel the request loop task and attempt a shutdown of the event loop.
        The method will block until the event loop stops.

        Returns:
            bool: True if successful, False if not.

        """
        if self.connection_state != ConnectionState.DISCONNECTED:
            if self.request_loop_task is not None:
                self.request_loop_task.cancel()
                self.request_loop_task = None
            if self.connect_task is not None:
                self.connect_task.cancel()
                self.connect_task = None
            self.log.debug("Waiting for event loop shutdown.")
            while self.loop is not None and self.loop.is_running():
                time.sleep(0.5)
                self.log.debug("Event loop status: {}.".format(self.loop.is_running()))
            self.log.info("Cleaning up remaining tasks.")
            asyncio.set_event_loop(self.loop)
            remaining = asyncio.all_tasks(self.loop)
            self.loop.run_until_complete(asyncio.gather(*remaining))
            self.loop.close()
            self.log.debug("Event loop closed.")
            self.loop = None
            self.set_connection_state(ConnectionState.DISCONNECTED)
            return True
        return False

    async def digest_loop(self):
        """Internal method. Calls the digest request on remote in a loop.

        """
        process_manager_digest_task = None
        share_manager_digest_task = None
        try:
            self.log.info("Starting digest loop.")
            while True:
                process_manager_digest_task = asyncio.ensure_future(self.digest_request(KrakenServerAgent.PROCESS_MANAGER))
                share_manager_digest_task = asyncio.ensure_future(self.digest_request(KrakenServerAgent.SHARE_MANAGER))

                # Sleep in intervals to quickly handle cancellations
                if self.data_interval > 0.2:
                    i = 0
                    while i < int(self.data_interval / 0.1):
                        await asyncio.sleep(0.1)
                        i += 1
                else:
                    await asyncio.sleep(self.data_interval)
        except asyncio.CancelledError:
            if process_manager_digest_task is not None:
                process_manager_digest_task.cancel()
            if share_manager_digest_task is not None:
                share_manager_digest_task.cancel()
            self.log.info("Digest loop cancelled.")

    async def digest_request(self, agent):
        """Internal method. Sends the actual digest call and emits a signal when the digest object
        is returned.

        Args:
            agent (KrakenServerAgent): The agent to request from.

        """
        t1 = time.perf_counter()
        try:
            if self.request_locks[agent].acquire(False):
                try:
                    self.log.debug("Requesting digest.")
                    digest = await self.agent.make_call(agent, "generate_digest", [])

                    if digest is not None:
                        if agent == KrakenServerAgent.SHARE_MANAGER:
                            self.share_manager_digest_cache = digest
                            self.data_signals.share_manager_digest_received.emit(deepcopy(digest))
                        elif agent == KrakenServerAgent.PROCESS_MANAGER:
                            self.process_manager_digest_cache = digest
                            self.data_signals.process_manager_digest_received.emit(deepcopy(digest))
                    else:
                        self.log.warning("Server refused to return data (are you polling too fast?).")
                finally:
                    self.request_locks[agent].release()
            else:
                self.log.debug("Digest request overlapping, and has been discarded.")
        except ConnectionResetError as error:
            self.log.error("Connection was reset: {}.".format(error))
            self.signals.error.emit("Connection was reset: {}.".format(error))
        except asyncio.CancelledError:
            self.log.info("Request task cancelled.")
        t2 = time.perf_counter()
        self.log.debug("Digest request took {} seconds.".format(t2 - t1))


class KrakenServerConnection(KrakenMonitorConnection):
    """Handles connecting and disconnecting to a remote server and performing data requests.

    Remote server connections are handled by starting an additional worker QThread. The thread
    holding this object will not be blocked. See ``KrakenServerWorker`` for more information.

    Args:
        parent (QObject, optional): Parent of this object. Defaults to None.

    Attributes:
        log (logging.Logger): Logger object.
        signals (KrakenServerWorker.Signals): Signals for the connection. Provided to the worker.
        worker (KrakenServerWorker): Worker object.
        thread (QThread): Thread the worker runs on.
        is_running (Property): Determines if the worker is running and has a connection.

    """
    class Signals(QObject):
        """Signals for ``KrakenServerConnection``.

        Attributes:
            process_manager_digest_received (dict): Digest data passed through this signal.
            share_manager_digest_received (dict): Digest data passed through this signal.

        """
        process_manager_digest_received = Signal(dict)
        share_manager_digest_received = Signal(dict)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.log = logging.getLogger(__file__)
        self.signals = KrakenServerConnection.Signals()
        self.worker = None
        self.thread = None
        self.data_interval = 1

    @Signal
    def connection_state_changed(self):
        """Signal used when the connection status changes.

        """
        pass

    def _connection_state(self):
        """Returns the running state of the worker

        Returns:
            bool: True if connected. False if not.

        """
        return ConnectionState.DISCONNECTED.value if self.worker is None else self.worker.connection_state.value

    connection_state = Property(int, _connection_state, notify=connection_state_changed)

    @Slot(float, result=bool)
    def set_data_interval(self, interval):
        """Change the data interval to poll for digest updates.

        Args:
            interval (float): The polling interval in seconds.

        Returns:
            bool: True if successful, False if not.

        """
        if interval >= 0:
            if self.worker is not None:
                self.worker.data_interval = interval
            self.data_interval = interval
            return True
        return False

    @Slot(str, int, result=bool)
    def start(self, remote_host, remote_port, remote_protocol="tcp", remote_id=0, data_interval=None, blosc=True):
        """Start the connection.

        This method will start the ``QThread`` and ``KrakenServerWorker``. Signals for both will
        be connected. Will stop any previously started workers.

        Args:
            remote_host (str): Hostname of the remote container.
            remote_port (int): Port of the remote container.
            remote_protocol (str, optional): Protocol for the remote container. Defaults to "tcp".
            remote_id (int, optional): Id of the remote agent. Defaults to 0.
            data_interval (int, optional): The interval to poll for data. Defaults to 1.
            blosc (bool, optional): Enable to use blosc compression. Must be the same as server
                configuration. Defaults to True.

        Returns:
            bool: True if successful, False if not.

        """
        self.stop()

        if self.worker is None:
            if data_interval is not None:
                self.data_interval = data_interval
            self.worker = KrakenServerWorker(self.signals, remote_host, remote_port, remote_protocol, self.data_interval, blosc)
            self.thread = QThread()
            self.worker.moveToThread(self.thread)
            self.worker.signals.error.connect(self.error_handler)
            self.worker.signals.connection_state_changed.connect(self.connection_state_changed)
            self.thread.started.connect(self.worker.run)
            self.thread.start()
            return True
        return False

    @Slot(bool, result=bool)
    def stop(self, blocking=True):
        """Stops the connection.

        Both the ``KrakenServerWorker`` and ``QThread`` objects will be stopped.

        Args:
            blocking (bool, optional): If True, will block current thread during the shutdown
                process. Defaults to True.

        Returns:
            bool: True if successful, False if not.

        """
        def sequence():
            if self.worker is not None:
                if self.worker.stop():
                    self.worker.signals.error.disconnect()
                    self.worker.signals.connection_state_changed.disconnect()
                self.worker = None

            if self.thread is not None:
                self.thread.exit()
                self.thread.wait(5000)
                self.thread = None
            return True

        if not blocking:
            stop_thread = Thread(target=sequence)
            stop_thread.start()
            return stop_thread
        else:
            return sequence()
        return False

    @Slot(str)
    def error_handler(self, message):
        """Handles error messages coming from the ``QThread``.

        Args:
            message (str): The error message.

        """
        self.log.error("Error from worker thread: {}.".format(message))
        self.stop(blocking=False)

    @Slot()
    def refresh_digest(self):
        """Calls ``refresh_digest`` on the worker.

        Args:
            agent (KrakenServerAgent): The agent to refresh digest on.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.refresh_digest()

    def process_run(self, filenames):
        """Calls ``process_run`` on the worker.

        Args:
            filenames (list): List of strings containing the shm filenames of the process.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.PROCESS_MANAGER, "process_run", [filenames])

    def process_step(self, filenames):
        """Calls ``process_step`` on the worker.

        Args:
            filenames (list): List of strings containing the shm filenames of the process.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.PROCESS_MANAGER, "process_step", [filenames])

    def process_pause(self, filenames):
        """Calls ``process_pause`` on the worker.

        Args:
            filenames (list): List of strings containing the shm filenames of the process.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.PROCESS_MANAGER, "process_pause", [filenames])

    def process_no_compute(self, filenames):
        """Calls ``process_no_compute`` on the worker.

        Args:
            filenames (list): List of strings containing the shm filenames of the process.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.PROCESS_MANAGER, "process_no_compute", [filenames])

    def process_exit(self, filenames):
        """Calls ``process_exit`` on the worker.

        Args:
            filenames (list): List of strings containing the shm filenames of the process.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.PROCESS_MANAGER, "process_exit", [filenames])

    def process_signal(self, signal, filenames):
        """Calls ``process_signal`` on the worker.

        Args:
            signal (int): The signal value to send. Can be obtained from ``signal`` module.
            filenames (list): List of strings containing the shm filenames of the process.

        Raises:
            AssertionError: If worker was not instatiated.

        Returns:
            bool: True if successful, False if not.

        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.PROCESS_MANAGER, "process_signal", [signal, filenames])

    def read_log(self, filename, from_byte, to_byte=None, callback=None):
        """Reads a log file.

        Args:
            filename (str): Full filename of the log file.
            from_byte (int): Start reading from this byte location.
            to_byte (int, optional): Read till this byte location. None will read everything.
                Defaults to None.
            callback (Callable, optional): A callback function to handle data. Defaults to None.
            callback_args (list, optional): A list of arguments to pass to the callback function.
                Defaults to [].

        Returns:
            bool: True if successful, False if not.
        """
        assert self.worker is not None
        self.log.debug("Reading {} from byte {} to byte {}.".format(filename, from_byte, to_byte))
        return self.worker.call_async(KrakenServerAgent.SHARE_MANAGER, "read_log", [filename, from_byte, to_byte], callback=callback)

    def read_kernel_info(self, filename, callback=None):
        """Reads a kernel file.

        Args:
            filename (str): Full filename of the kernel file.
            callback (Callable, optional): A callback function to handle data. Defaults to None.
            callback_args (list, optional): A list of arguments to pass to the callback function.
                Defaults to [].

        Returns:
            bool: True if successful, False if not.
        """
        assert self.worker is not None
        return self.worker.call_async(KrakenServerAgent.SHARE_MANAGER, "read_kernel_info", [filename], callback=callback)
