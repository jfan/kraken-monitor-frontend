import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

Dialog {
    property string serverAddress: ""
    property int serverPort: 20000
    property bool valid: false

    function validateFields() {
        if (serverAddressField.acceptableInput && serverPortField.acceptableInput) {
            buttons.standardButton(Dialog.Ok).enabled = true;
            valid = true;
        }
        else {
            buttons.standardButton(Dialog.Ok).enabled = false;
            valid = false;
        }
    }

    function enterIfValid() {
        if (valid) {
            accept();
        }
    }

    modal: true

    onAccepted: {
        serverAddress = serverAddressField.text.toString();
        serverPort = parseInt(serverPortField.text);
        dataConnection.start(serverAddress, serverPort);
    }

    onOpened: {
        serverAddressField.text = serverAddress.toString();
        serverPortField.text = serverPort.toString();
        validateFields();
        Qt.callLater(serverAddressField.forceActiveFocus);
    }

    footer: DialogButtonBox {
        id: buttons
        standardButtons: Dialog.Ok | Dialog.Cancel
    }

    Item {
        anchors.fill: parent
        ColumnLayout {
            anchors.fill: parent
            Label {
                Layout.fillWidth: true
                Layout.bottomMargin: 10
                text: "<b>Enter Kraken Server details</b>"
                font.pixelSize: 16
            }
            TextField {
                id: serverAddressField
                Layout.fillWidth: true
                text: ""
                validator: RegExpValidator {
                    // The hostname regexp https://stackoverflow.com/questions/1418423/the-hostname-regex
                    regExp: /^(?=.{1,255}$)[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?(?:\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?)*\.?$/
                }
                selectByMouse: true
                placeholderText: "Server address (e.g. www.serveraddress.com)"
                onTextChanged: {
                    validateFields();
                }
                onAccepted: {
                    enterIfValid();
                }
            }
            TextField {
                id: serverPortField
                Layout.fillWidth: true
                text: "20000"
                placeholderText: "Server port (e.g. 20000)"
                validator: IntValidator {
                    bottom: 1
                    top: 65535
                }
                selectByMouse: true
                onTextChanged: {
                    validateFields();
                }
                onAccepted: {
                    enterIfValid();
                }
            }
        }
    }
}
