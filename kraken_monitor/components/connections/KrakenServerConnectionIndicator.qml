import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12


Item {
    property string indicatorColor: {
        switch (dataConnection.connection_state) {
            case 0:
            return Material.color(Material.Green);
            case 1:
            return Material.color(Material.Red)
            case 2:
            return Material.color(Material.Amber);
            default:
            return Material.color(Material.Grey)
        }

    }

    property string indicatorText: {
        switch (dataConnection.connection_state) {
            case 0:
            return "Connected";
            case 1:
            return "Disconnected";
            case 2:
            return "Connecting";
            default:
            return "Unknown"
        }
    }

    RowLayout {
        anchors.fill: parent
        spacing: 10
        Item {
            Layout.fillHeight: true
            Layout.preferredWidth: 15
            Rectangle {
                anchors.centerIn: parent
                color: Material.shade(indicatorColor, Material.Shade300)
                border.color: indicatorColor
                border.width: 1
                width: parent.width
                height: parent.width
                radius: width * 0.5
            }
        }

        Label {
            topPadding: 1
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.rightMargin: 15
            verticalAlignment: Qt.AlignVCenter
            font.pixelSize: 16
            text: indicatorText
        }
    }
}
