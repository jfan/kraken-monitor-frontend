"""Provides connections to a data source.

These connections are used within bridges to drive their displays. The data retrieved from
connections will be transformed into a suitable form for insertion into a model.

"""

from .kraken_server_connection import KrakenServerConnection, KrakenServerAgent, ConnectionState
