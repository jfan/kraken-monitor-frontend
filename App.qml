/*** Provides the application window.

***/


import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

import 'kraken_monitor/components'
import 'kraken_monitor/components/connections'
import 'kraken_monitor/components/dashboards'
import 'kraken_monitor/components/dashboards/details'
import 'kraken_monitor/components/dashboards/processes'
import 'kraken_monitor/components/dashboards/share'
import 'kraken_monitor/components/dashboards/system'
import 'kraken_monitor/components/dashboards/timing'

ApplicationWindow {
    id: window
    visible: true
    title: "Kraken Monitor"
    minimumHeight: 640
    minimumWidth: 800

    Material.theme: Material.Light
    Material.accent: Material.Indigo
    Material.background: "white"

    function colorAlpha(color, alpha) {
        return Qt.rgba(color.r, color.g, color.b, alpha);
    }

    Component.onCompleted: {
        window.flags |= Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMinMaxButtonsHint | Qt.WindowCloseButtonHint
    }

    menuBar: RowLayout {
        anchors.fill: parent.fill
        spacing: 0
        MenuBar {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Menu {
                title: qsTr("File")
                Action {
                    text: dataConnection.connection_state === 1 ? qsTr("Connect") : qsTr("Disconnect")
                    enabled: dataConnection.connection_state !== 2
                    onTriggered: {
                        if (dataConnection.connection_state === 1) {
                            connectionDialog.open();
                        }
                        else {
                            dataConnection.stop(false);
                        }
                    }
                }
                MenuSeparator { }
                Action {
                    text: qsTr("Quit")
                    onTriggered: {
                        Qt.quit()
                    }
                }
            }
            Menu {
                title: qsTr("Options")
                Action {
                    text: qsTr("Always on top")
                    checkable: true
                    onTriggered: {
                        if (checked) {
                            window.flags |= Qt.WindowStaysOnTopHint;
                        }
                        else {
                            window.flags ^= Qt.WindowStaysOnTopHint;
                        }
                    }
                }
            }
            Menu {
                title: qsTr("View")
                Action {
                    text: qsTr("Refresh now")
                    enabled: dataConnection.connection_state === 0
                    onTriggered: {
                        if (dataConnection.connection_state === 0) {
                            dataConnection.refresh_digest();
                        }
                    }
                }
                Menu {
                    id: menuUpdateSpeed

                    function clearChecked() {
                        stoppedOption.checked = false;
                        slowOption.checked = false;
                        mediumOption.checked = false;
                        fastOption.checked = false;
                    }

                    title: qsTr("Update speed")
                    Action {
                        id: stoppedOption
                        text: qsTr("Stopped")
                        checkable: true
                        onTriggered: {
                            menuUpdateSpeed.clearChecked();
                            checked = true;
                            dataConnection.set_data_interval(Number.MAX_SAFE_INTEGER)
                        }
                    }

                    Action {
                        id: slowOption
                        text: qsTr("Slow")
                        checked: true
                        checkable: true
                        onTriggered: {
                            menuUpdateSpeed.clearChecked();
                            checked = true;
                            dataConnection.set_data_interval(1)
                        }
                    }
                    Action {
                        id: mediumOption
                        text: qsTr("Medium")
                        checkable: true
                        onTriggered: {
                            menuUpdateSpeed.clearChecked();
                            checked = true;
                            dataConnection.set_data_interval(0.5)
                        }
                    }
                    Action {
                        id: fastOption
                        text: qsTr("Fast")
                        checkable: true
                        onTriggered: {
                            menuUpdateSpeed.clearChecked();
                            checked = true;
                            dataConnection.set_data_interval(0.2)
                        }
                    }
                }
            }
            Menu {
                title: qsTr("Help")
                Action {
                    text: qsTr("About")
                    onTriggered: {
                        aboutDialog.open();
                    }
                }
            }
        }
        KrakenServerConnectionIndicator {
            Layout.preferredWidth: 150
            Layout.fillHeight: true
        }
    }

    AboutDialog {
        id: aboutDialog
        width: 700
        height: 550
        anchors.centerIn: parent
    }

    KrakenServerConnectionDialog {
        id: connectionDialog
        width: 400
        height: 200
        anchors.centerIn: parent
    }

    Pane {
        id: body
        padding: 0
        anchors.fill: parent

        Dashboard {
            Processes {}
            Details {}
            Share {}
//            Timing {}
//            System {}
        }
    }
}
