#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Kraken Monitor - GUI application for Kraken process monitoring and control.

This file is the entrypoint for starting the Kraken Monitor application via command line. It
provides a range of connection and logging options in its arguments. For more information, run
python run_app.py --help.

Examples:

    $ python run_app.py
    $ python run_app.py --remote-host 0.0.0.0 --polling-interval 0.5 --verbose
"""


import sys
import os
import argparse
import logging
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtGui import QGuiApplication
from kraken_monitor.components.connections import KrakenServerConnection
from kraken_monitor.components.dashboards.details import DetailsBridge, DetailsListModel
from kraken_monitor.components.dashboards.processes import ProcessesBridge, ProcessesListModel
from kraken_monitor.components.dashboards.share import ShareBridge, ShareLogsListModel, ShareKernelsListModel


if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(description="Kraken Monitor - GUI application for Kraken process monitoring and control.")
    parser.add_argument("-p", "--remote-port", default=20000, type=int, help="Specify RPC server port. Defaults to 20000.")
    parser.add_argument("-a", "--remote-host", default="", type=str, help="Specify RPC server bind hostname.")
    # parser.add_argument("-b", "--no-blosc", action="store_false", help="Disable blosc compression for data transport.")
    parser.add_argument("-i", "--polling-interval", default=None, type=float, help="Frequency to poll for process data (e.g. 1 s). Recommended > 0.05.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Enable logging (logging.INFO).")
    parser.add_argument("-vv", "--extra-verbose", action="store_true", help="Enable debug logging (logging.DEBUG).")
    parser.add_argument("-q", "--quiet", action="store_true", help="Disable logging (logging.CRITICAL).")
    parser.add_argument("--log-file", default=None, type=str, help="Specify a log file to stream logging data.")
    args = parser.parse_args()

    # Setup logging
    log = logging.getLogger(__file__)

    if args.quiet:
        log_level = logging.CRITICAL
    if args.extra_verbose:
        log_level = logging.DEBUG
    elif args.verbose:
        log_level = logging.INFO
    else:
        log_level = logging.WARNING

    if args.log_file is not None:
        log_file = os.path.abspath(args.log_file)
    else:
        log_file = None

    logging.basicConfig(filename=log_file, filemode="a", level=log_level)

    # Setup Python components
    log.debug("Setting up python components.")
    connection = KrakenServerConnection()

    # Bootstrap Qt Application
    log.debug("Bootstrapping Qt application.")
    os.environ["QT_QUICK_CONTROLS_1_STYLE"] = "Base"  # Set base style for Qt Quick 1
    app = QGuiApplication(sys.argv + ["--style", "material"])  # Set Material style for Qt Quick 2
    engine = QQmlApplicationEngine()
    engine.addImportPath(os.path.join(os.path.dirname(__file__)))

    processes_model = ProcessesListModel()
    processes_bridge = ProcessesBridge(connection, processes_model)
    details_model = DetailsListModel()
    details_bridge = DetailsBridge(connection, details_model)
    share_kernels_model = ShareKernelsListModel()
    share_logs_model = ShareLogsListModel()
    share_bridge = ShareBridge(connection, share_logs_model, share_kernels_model)

    log.debug("Injecting objects into Qt context.")
    engine.rootContext().setContextProperty("processesBridge", processes_bridge)
    engine.rootContext().setContextProperty("processesListModel", processes_model)
    engine.rootContext().setContextProperty("detailsBridge", details_bridge)
    engine.rootContext().setContextProperty("detailsListModel", details_model)
    engine.rootContext().setContextProperty("shareBridge", share_bridge)
    engine.rootContext().setContextProperty("shareLogsModel", share_logs_model)
    engine.rootContext().setContextProperty("shareKernelsModel", share_kernels_model)
    engine.rootContext().setContextProperty("dataConnection", connection)

    log.debug("Loading QML file.")
    engine.load(os.path.join(os.path.dirname(__file__), "App.qml"))
    os.environ["QT_QUICK_CONTROLS_1_STYLE"] = ""

    if not engine.rootObjects():
        log.error("Root object not found, exiting.")
        sys.exit(-1)

    # Start application
    if args.remote_host:
        # Only connect if a host is specified, otherwise the connection is done after startup by the user
        connection.start(args.remote_host, args.remote_port, data_interval=args.polling_interval)  # , blosc=not args.no_blosc)
    processes_bridge.start()
    details_bridge.start(paused=True)
    share_bridge.start(paused=True)

    # Blocking execution loop
    log.debug("Executing Qt event loop.")

    app.exec_()
    log.debug("Exiting application.")

    # Stop QML from emitting errors by destroying views first
    del engine
    del app

    # Use async stop
    for i, t in enumerate([processes_bridge.stop(False), details_bridge.stop(False), share_bridge.stop(False)]):
        try:
            t.join()
        except AttributeError:
            log.warning("Bridge {} has already stopped.".format(i))

    connection.stop()

    log.debug("Waiting for exit.")
    sys.exit(0)
