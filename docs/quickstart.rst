Quickstart
==========

Make sure you have Python 3.7+ installed. Kraken Server should be running on a machine that has 
OCEAN setup, and the Kraken Manager should have been used to start the processes.

Qt Installation
---------------

The Kraken Monitor GUI is powered by Qt 5.12+. Installation of Qt is thus required. See 
`Qt Downloads`_ for instructions on installing Qt.

.. _`Qt Downloads`: https://www.qt.io/download-open-source

Python Packages
---------------

Install the required pip packages by running:

.. code-block:: text

    pip install -r requirements.txt

If developing this tool, install the dev dependencies by running:

.. code-block:: text

    pip install -r requirements-dev.txt

Running the Application
-----------------------

To start the application, simply provide the ``run_app`` script:

.. code-block:: text

    python3 run_app.py

Connections are then made through the *connect* option in the *File* menu.

Additional command line options can be accessed via ``--help``:

.. code-block:: text

    usage: run_app.py [-h] [-p REMOTE_PORT] -a REMOTE_HOST [-b]
                  [-i POLLING_INTERVAL] [-v] [-vv] [-q] [--log-file LOG_FILE]

    Kraken Monitor - GUI application for Kraken process monitoring and control.

    optional arguments:
    -h, --help            show this help message and exit
    -p REMOTE_PORT, --remote-port REMOTE_PORT
                            Specify RPC server port. Dedaults to 20000.
    -a REMOTE_HOST, --remote-host REMOTE_HOST
                            Specify RPC server bind hostname.
    -i POLLING_INTERVAL, --polling-interval POLLING_INTERVAL
                            Frequency to poll for process data (e.g. 1 s).
                            Recommended > 0.05.
    -v, --verbose         Enable logging (logging.INFO).
    -vv, --extra-verbose  Enable debug logging (logging.DEBUG).
    -q, --quiet           Disable logging (logging.CRITICAL).
    --log-file LOG_FILE   Specify a log file to stream logging data.
