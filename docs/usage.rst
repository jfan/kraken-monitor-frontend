Usage
=====

This page will help explain the operation of the application.

Connection
----------

The current connection status is shown with an indicator on the top right of the application. There
are three connection states:

* Connected (Green)
* Disconnected (Red)
* Connecting (Orange)

Processes
---------

This is the user friendly view of the Kraken Monitor. This view displays process groups with
their ``MarlinBU`` and ``MarlinConfig`` parts under one entry. Control commands issued to the group
would only affect the control status of the ``MarlinBU``. Stop command issued to the group will
send SIGTERM to all processes in the group.

Each column in the list is formatted differently based on their content. Two reduction modes are
used; ``first`` and ``sum``. The ``first`` reduction mode will take the content from the first
listed process in the group. The ``sum`` reduction mode is commonly used to take content from all
the processes in the group and perform a summation of their content.

============    ======================
Property        Reduction Method
============    ======================
Name            First
Status          First
Control         First
CPU             Sum
Memory          Sum
Loop Count      First
============    ======================

Color labelling exists for the ``Status`` and ``Control`` properties. Green is used for both ALIVE
and RUNNING/STEP whilst red and amber are used for DEAD and PAUSED respectively.

Details
-------

This is the advanced user view of the KrakenMonitor. This view displays processes individually
along with all available properties. Some pretty printing is used to turn numbers into a more
readable format.

Columns can be moved and resized in this view.

Share
-----

This view allows accessing logs and kernel connection files on the server. A text view is provided
of these files.

To view a log, double click on the row entry.

To connect to a kernel, double click on the row entry.

These files are not automatically cleaned up on the server after processes exit.
