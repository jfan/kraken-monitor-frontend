Welcome to KrakenMonitor's documentation!
=========================================

Kraken Monitor is the tool developed under the AITC Summer Research scholarship for the control of 
processes spawned by Kraken. This is a GUI interface that allows the pausing, starting, stepping and 
stopping of processes. The code files are written in Python with PySide2 bindings to Qt 5.

Currently, the tabbed interface has two completed pages. The details view will allow advanced users 
to see all the properties of each process. The processes view is a more user friendly view that 
displays relevant properties for each process group.

The interface connects to a Kraken Server backend to retrieve data from a host machine running the 
processes. The communication between frontend and backend is through RPC with ``aiomas``. msgpack
encoding with blosc compression is used.

This tool was authored by Jerry Fan.


.. toctree::
    :maxdepth: 4

    quickstart
    usage
    reference/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
