``share_file_manager`` Module
=============================

.. automodule:: kraken_monitor.components.dashboards.share.share_file_manager
   :members:
   :undoc-members:
   :show-inheritance:
