``share_logs_list_model`` Module
===================================

.. automodule:: kraken_monitor.components.dashboards.share.share_logs_list_model
   :members:
   :undoc-members:
   :show-inheritance:
