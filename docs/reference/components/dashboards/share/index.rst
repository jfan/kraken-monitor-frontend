``components.dashboards.share`` Package
===========================================

.. automodule:: kraken_monitor.components.dashboards.share
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::
    :maxdepth: 2

    share_bridge
    share_kernels_list_model
    share_logs_list_model
    share_file_manager
