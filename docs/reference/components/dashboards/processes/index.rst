``components.dashboards.processes`` Package
===========================================

.. automodule:: kraken_monitor.components.dashboards.processes
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::
    :maxdepth: 2

    processes_bridge
    processes_list_model
