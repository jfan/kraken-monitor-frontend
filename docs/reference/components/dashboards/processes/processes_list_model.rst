``processes_list_model`` Module
===============================

.. automodule:: kraken_monitor.components.dashboards.processes.processes_list_model
   :members:
   :undoc-members:
   :show-inheritance:
