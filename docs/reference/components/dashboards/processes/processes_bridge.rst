``processes_bridge`` Module
===========================

.. automodule:: kraken_monitor.components.dashboards.processes.processes_bridge
   :members:
   :undoc-members:
   :show-inheritance:
