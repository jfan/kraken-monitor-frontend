``details_bridge`` Module
===================================

.. automodule:: kraken_monitor.components.dashboards.details.details_bridge
   :members:
   :undoc-members:
   :show-inheritance:
