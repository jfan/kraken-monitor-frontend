``details_list_model`` Module
===================================

.. automodule:: kraken_monitor.components.dashboards.details.details_list_model
   :members:
   :undoc-members:
   :show-inheritance:
