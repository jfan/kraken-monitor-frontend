``components.dashboards.details`` Package
=========================================

.. automodule:: kraken_monitor.components.dashboards.details
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::
    :maxdepth: 2

    details_bridge
    details_list_model
